#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

import pygimli as g
from pygimli.viewer import showMesh
from pygimli.polytools import *
from pygimli.meshtools import appendTriangleBoundary

import pybert as bert
import pylab as P

import numpy as np

def unitTest( iMesh, name ):

    mesh = g.Mesh( iMesh )
    for n in mesh.nodes():
        if n.marker() == -99: n.setMarker( 0 )

    for b in mesh.boundaries():
        if b.marker() == g.MARKER_BOUND_MIXED:
            b.setMarker( g.MARKER_BOUND_HOMOGEN_NEUMANN )
        if b.shape().norm()[2] == -1.:
           if b.marker() < 0: b.setMarker( -10001 )
        if b.shape().norm()[2] == 1.:
            if b.marker() < 0: b.setMarker( g.MARKER_BOUND_HOMOGEN_DIRICHLET )

    fop = bert.DCMultiElectrodeModelling( mesh, False )

    data = bert.DataContainerERT()
    data.createSensor( g.RVector3( 0.5, 0.5 , 0.0 ) )

    fop.calculate( data )
    pot = fop.solution()

    mesh.addExportData( 'u', pot[ 0] )

    mesh.exportVTK( name+'Unit' )

#def unitTest( ... )

def testCreateTetMesh( verbose = True ):
    polyCreateWorld( "tet", 1, 1, 1, marker = 1, maxArea = 0.001, verbose = verbose )
    polyAddVIP( "tet", [0.0, 0.0, 0.0], marker = -99, isRegionMarker = False, isHoleMarker = False, verbose =  verbose)
    polyAddVIP( "tet", [0.001, 0.001, 0.0], marker = 0, isRegionMarker = False, isHoleMarker = False, verbose =  verbose)
    mesh = tetgen( "tet", quality = 1.12, preserveBoundary = False, verbose =  verbose)

    mesh.translate( g.RVector3( 0.5, 0.5)  )

    return mesh

def testCreateHexMesh( verbose = True ):
    grid = g.createMesh2D( g.asvector( np.linspace(0, 1, 11 )) ,g.asvector( np.linspace(0, 1, 11 ) ) )
    for n in grid.nodes():
        if n.pos() == g.RVector3( 0.5, 0.5, 0 ):
            n.setMarker( -99 )

    for c in grid.cells(): c.setMarker( 1 )

    mesh3 = g.createMesh3D( grid, g.asvector( np.linspace(0, -1, 12 ) ), g.MARKER_BOUND_HOMOGEN_NEUMANN, g.MARKER_BOUND_MIXED )

    for b in mesh3.boundaries():
        if b.marker() == 1:
            b.setMarker( g.MARKER_BOUND_MIXED )

    return mesh3

def testCreatePriMesh( verbose = True ):
    poly = g.Mesh( 2 )
    n0 = poly.createNode( 0.0, 0.0, 0. )
    n1 = poly.createNode( 1.0, 0.0, 0. )
    n2 = poly.createNode( 0.0, 1.0, 0. )
    n3 = poly.createNode( 1.0, 1.0, 0. )
    n4 = poly.createNode( 0.5, 0.5, 0., -99 )
    #n5 = poly.createNode( 0.501, 0.501, 0. )
    poly.createEdge( n0, n1, g.MARKER_BOUND_MIXED )
    poly.createEdge( n1, n3, g.MARKER_BOUND_MIXED )
    poly.createEdge( n3, n2, g.MARKER_BOUND_MIXED )
    poly.createEdge( n2, n0, g.MARKER_BOUND_MIXED )

    mesh2 = g.Mesh( 2 )
    g.TriangleWrapper( poly, mesh2, "-pzeAfa0.01q34Q" );

    for c in mesh2.cells(): c.setMarker( 1 )

    #print mesh2
    #showMesh( mesh2 )

    mesh3 = g.createMesh3D( mesh2, g.asvector( np.linspace(0, -1, 21 ) ), g.MARKER_BOUND_HOMOGEN_NEUMANN, g.MARKER_BOUND_MIXED )
    #print mesh3

    return mesh3
# def testCreateTriPrismMesh( ... )

def testCreateMixMesh( verbose = True ):
    grid = g.createMesh2D( g.asvector( np.linspace( 0.25, 0.75, 11 ) ) ,g.asvector( np.linspace( 0.25, 0.75, 11 ) ) )

    for n in grid.nodes():
        if n.pos() == g.RVector3( 0.5, 0.5, 0 ):
            n.setMarker( -99 )

    for b in grid.boundaries():
        b.setMarker( 0 )

    grid.translate( g.RVector3( 0.0, -1.0 ) )

    #grid.cell( 76 ).node( 0).setPos( grid.cell( 76 ).node( 0).pos() *0.97 )
    mesh2 = appendTriangleBoundary( grid, xbound = 0.25, ybound = 0.25, marker = 1, isSubSurface = True)
    mesh2.translate( g.RVector3( 0.0, 1.0 ) )



    for b in mesh2.boundaries():
        if b.marker() < 0:
            b.setMarker( g.MARKER_BOUND_MIXED )

    #showMesh( mesh2.createH2() )

    for c in mesh2.cells(): c.setMarker( 1 )

    #showMesh( mesh2 )
    mesh3 = g.createMesh3D( mesh2, g.asvector( np.linspace(0, -1, 21 ) ), g.MARKER_BOUND_HOMOGEN_NEUMANN, g.MARKER_BOUND_MIXED )

    return mesh3

def testMesh( mesh, name, uTest, p2, h2 = False ):

    if h2:
        mesh = mesh.createH2()
        name = name + "H2"
    if p2:
        mesh = mesh.createP2()
        name = name + "P2"

    if uTest:
        unitTest( mesh, name )

    mesh.createNeighbourInfos()
    print name, mesh

    cell = mesh.findCell( g.RVector3( 0.5, 0.5, -0.5 ) )
    #print cell


    mesh.setCellAttributes( g.RVector( mesh.cellCount(), 1.0 ) )
    fop = bert.DCMultiElectrodeModelling( mesh, False )

    subPotentials = g.RMatrix()
    fop.collectSubPotentials( subPotentials )

    data = bert.DataContainerERT()
    data.createSensor( g.RVector3( 0.5, 0.5 , 0.0 ) )
    data.createSensor( g.RVector3( 0.5, 0.5 , -1.0 ) )

    swatch = g.Stopwatch( True )
    fop.calculate( data )
    pot = fop.solution()

    #print "u min: ", min(pot[0]), "u max: ", max( pot[0]) , "time: ", swatch.duration() , " s"
    print "u min: ", min(pot[0]), "u max: ", max( pot[0]) , "time: ", swatch.duration() , " s", "err: ", (1.-(min(pot[0])/uExact))*100., '%'

    #print g.find( pot[0] == min(pot[0]) ), g.find( pot[0] < 0 )
    #tmpC = g.stdVectorI()
    #for c in mesh.node( g.find( pot[0] < 0 )[0] ).cellSet():
    #print c.id()
    #tmpC.append( c.id() )
    #tmp = g.Mesh(3)
    #tmp.createMeshByCellIdx( mesh, tmpC )
    #tmp.exportVTK( "fail")

    mesh.addExportData( 'u', pot[0] )
    if ( min( pot[ 0 ] ) > 1e-12 ): mesh.addExportData( 'log10( u )', g.log10( pot[0] ) )

    mesh.save( name )
    mesh.exportVTK( name )
    mesh.exportBoundaryVTU( name + 'Bound' )
# def testMesh( ... )


uTest = False;


uExact = 1. / (2.*P.pi) * 1./P.sqrt( 0.5**2 + 0.5**2 + 1. )
print "umin-Exact: ", uExact


testMesh( testCreateTetMesh( verbose = False ), 'tet', uTest = uTest, p2 = False, h2 = False )
testMesh( testCreateTetMesh( verbose = False ), 'tet', uTest = uTest, p2 = False, h2 = True )
testMesh( testCreateTetMesh( verbose = False ), 'tet', uTest = uTest, p2 = True , h2 = False )

testMesh( testCreateHexMesh( verbose = False ), 'hex', uTest = uTest, p2 = False, h2 = False )
testMesh( testCreateHexMesh( verbose = False ), 'hex', uTest = uTest, p2 = False, h2 = True )
testMesh( testCreateHexMesh( verbose = False ), 'hex', uTest = uTest, p2 = True, h2 = False )

testMesh( testCreatePriMesh( verbose = False ), 'pri', uTest = uTest, p2 = False, h2 = False )
testMesh( testCreatePriMesh( verbose = False ), 'pri', uTest = uTest, p2 = False, h2 = True )
testMesh( testCreatePriMesh( verbose = False ), 'pri', uTest = uTest, p2 = True, h2 = False )

testMesh( testCreateMixMesh( verbose = False ), 'mix', uTest = uTest, p2 = False, h2 = False )
testMesh( testCreateMixMesh( verbose = False ), 'mix', uTest = uTest, p2 = False, h2 = True )
testMesh( testCreateMixMesh( verbose = False ), 'mix', uTest = uTest, p2 = True, h2 = False )
