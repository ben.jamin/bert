/***************************************************************************
 *   Copyright (C) 2007-2016 by the resistivity.net development team       *
 *   Carsten Rücker carsten@resistivity.net                                *
 *   Thomas Günther  thomas@resistivity.net                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <bert.h>

#include <optionmap.h>
#include <datamap.h>

using namespace GIMLI;

#include <string>

int main(int argc, char * argv[ ]) {

    //** define default values;
    bool verbose = false, estimateError = false, needsRhoaErrK = false, forceAbsRhoA = false,
        addGeometricFactor = false;
    bool noisify = false, trimMeanRhoA  = false, showStat = false, downWeight = false, deleteUnusedElecs = false;
    bool noFilter = false, sortData = false, averageDups = false, haveTopoEffect = false;
    bool sortX = false, inplace = false;
    bool forceFlatEarth = false;
    double errPerc        = 1.0;    // 1percent
    double errVolt        = 100e-6; // 0.1mV
    double defaultCurrent = 100e-3; // 100mA
    std::vector < std::string > filterRules;

    std::string dataFileName          = NOT_DEFINED;
    std::string outputFileName        = NOT_DEFINED;
    std::string correctFileName       = NOT_DEFINED;
    std::string outputFileFormat      = NOT_DEFINED;
    std::string outputFileFormatElecs = NOT_DEFINED;
    std::string collectFile           = NOT_DEFINED;

    OptionMap oMap;
    oMap.setDescription("DCEdit - Edit DC resistivity data set by filtering, error estimation or adding noise.\n");
    oMap.addLastArg(dataFileName,      "Data file");
    oMap.add(verbose            ,      "v"  , "verbose"         , "Verbose mode.");
    oMap.add(addGeometricFactor ,      "G"  , "addGeometricFactor", "Add geometric factor to the output list.");
    oMap.add(estimateError      ,      "E"  , "estimateError"   , "Estimate Error using error level+voltage.");
    oMap.add(forceAbsRhoA       ,      "A"  , "absoluteRhoA"    , "Force absolute apparent resistivity.");
    oMap.add(noisify            ,      "N"  , "noisify"         , "Add Gaussian noise using error level+voltage.");
    oMap.add(trimMeanRhoA       ,      "T"  , "trimmean"        , "Trim rhoa about (mean +/- 3 std) log rhoa.");
    oMap.add(showStat           ,      "S"  , "statistic"       , "Show some data statistics.");
    oMap.add(downWeight         ,      "D"  , "downWeight"      , "Downweight data instead of deleting it.");
    oMap.add(deleteUnusedElecs  ,      "U"  , "delUnusedElecs"  , "Delete unused electrodes.");
    oMap.add(needsRhoaErrK      ,      "B"  , "needsRhoaErr"    , "Needs rhoa/error (forces estimation if none present).");
    oMap.add(correctFileName    ,      "c:" , "correctFile"     , "Correct with geometric factor from a 2nd datafile");
    oMap.add(outputFileFormat   ,      "f:" , "outputFormat"    , "Format for output data [default=as input]. 'all' saves all data inclusive invalid.");
    oMap.add(outputFileFormatElecs,    "x:" , "outputCoords"    , "Format for output coords [default=as input].");
    oMap.add(outputFileName     ,      "o:" , "output"          , "Output filename.");
    oMap.add(inplace            ,      ""   , "inplace"         , "Replace orignal file. If you know what you do.");
    oMap.add(errPerc            ,      "e:" , "errorLevel"      , "Input error level in %.");
    oMap.add(errVolt            ,      "u:" , "voltageError"    , "Input voltage error in volts.");
    oMap.add(defaultCurrent     ,      "i:" , "defaultCurrent"  , "Input current for error estimation");
    oMap.add(collectFile        ,      ":"  , "collect"         , "Collect u for data from datamap file (collect file)");
    oMap.add(sortData           ,      ""   , "sort"            , "Sort the data regarding a b m n");
    oMap.add(sortX              ,      ""   , "sortX"           , "Sort the sensor positions regarding the x coordinate");
    oMap.add(averageDups        ,      ""   , "averageDups"     , "Merge duplicated Data by averaging. Data will be sorted as well.");
    oMap.add(noFilter           ,      ""   , "noFilter"        , "omit default data filter");
    oMap.add(forceFlatEarth     ,      ""   , "forceFlatEarth"  , "Calculate geometricFactor with z=0.");
    oMap.add(filterRules        ,      ":"  , "delete"             , "--delete='var OPERATOR val:A' Remove or downweight all values that match the rule. e.g., --filter='k>100' , marks all data with k > 100 as invalid and omits saving them. Arguments following an optional :A, will filter absolute values of data. You can repeat the filter option multiple times. Valid operators so far are: <; >; =. Do not add spaces inside the filter rule. Please note, if you want to filter for electrode IDs, you have to subtract 1 to the values inside the datafile since the electrodes are stored internally from 0 to nElectrodes -1. If you want to remove unused electrodes apply option -U. e.g. completely remove the first electrode from the datafile, apply: --filter='a=0' --filter='b=0' --filter='m=0' --filter='n=0' -U");
    oMap.add(filterRules        ,      ":"  , "filter"         , "DEPRECATED Please use --delete instead. --filter will be removed in the Future");

    oMap.parse(argc, argv);

    if (verbose){
        std::cout << "Filter: (downWeight) (" << downWeight << ")" << std::endl;

        for (size_t i = 0; i < filterRules.size(); i ++){
            std::cout << filterRules[i] << std::endl;
        }
    }

    if (needsRhoaErrK) addGeometricFactor = true;
    if (outputFileName == NOT_DEFINED) outputFileName = dataFileName + ".edt";
    if (inplace == true) outputFileName = dataFileName;

    DataContainerERT data(dataFileName, !noFilter);

    if (collectFile != NOT_DEFINED){
        DataMap dMap(collectFile);
        data.set("u", dMap.data(data));
        data.set("i", RVector(data.size(), 1.0));
        data.set("r", data("u"));
        data.set("rhoa", RVector(data.size(), 0.0));
        std::cout << "Filter data from collect file: " << collectFile << std::endl;
    }

    if (verbose) {
        std::cout << "Read: " << dataFileName << " with " << data.sensorCount()
                  << " electrodes and " << data.size() << " data." <<  std::endl;
    }

    //** correct data with alternative geometric factor
    if (correctFileName != NOT_DEFINED) {
        std::cout << "Checking correctfile: " << correctFileName << "..." << std::endl;

        RVector kCorrect(data.size());

        if (correctFileName.rfind(".collect") != std::string::npos) {

            if (verbose) std::cout << "create k from collect file " << std::endl;

            DataMap dMap(correctFileName);
            kCorrect = (1.0 / dMap.data(data));
        } else { //** data file containing geometric factor or voltage for 1 S/m

            if (verbose) std::cout << " read from datafile " << std::endl;

            DataContainerERT correctData(correctFileName);

            if (correctData.size() == data.size()) {
                if (correctData.allNonZero("k")) {

                    if (verbose) std::cout << "read k(correct) from data file" << std::endl;
                    kCorrect = correctData("k");

                } else if (correctData.allNonZero("r")){

                    if (verbose) std::cout << "create k(correct) from 1/r" << std::endl;
                    kCorrect = 1.0 / correctData("r");

                } else if (correctData.allNonZero("u")){

                    if (verbose) std::cout << "create k(correct) from 1/u (assuming 1 = 1A)" << std::endl;
                    kCorrect = 1.0 / correctData("u");

                    if (correctData.allNonZero("i")){

                        if (verbose) std::cout << "create k(correct) from 1/u/i" << std::endl;
                        kCorrect /= correctData("i");

                    }
                } else {
                    std::cerr << "cant create a valid k(correct) for: " << correctFileName << std::endl;
                    return EXIT_FAILURE;
                }
            } else {
                std::cerr << "data file size mismatch " << correctData.size() << " != " << data.size() << std::endl;
                return EXIT_FAILURE;
            }
        }

        //** we need r within the data file
        if (!data.allNonZero("r")) {
            if (data.allNonZero("rhoa")){
                if (data.allNonZero("k")){
                    if (verbose) std::cout << "found k in file" << std::endl;
                } else {
                    if (verbose) std::cout << "create k from geometricFactor(data)" << std::endl;

                    if (::fabs(min(z(data.sensorPositions())) -
                               max(z(data.sensorPositions())) < TOLERANCE &&
                            max(z(data.sensorPositions())) < 0.0)){
                        data.set("k", geometricFactors(data));

                    } else {
                        if (verbose) std::cout << "maybe found topography: omiting z-coordinate for geometricFactor(data)" << std::endl;

                        std::vector < RVector3 > tmp(data.sensorPositions());

                        for (Index i = 0; i < data.sensorCount(); i ++){
                            RVector3 p(data.sensorPosition(i)); p[2] = 0.0;
                            data.setSensorPosition(i, p);
                        }

                        data.set("k", geometricFactors(data));
                        data.setSensorPositions(tmp);
                    }
                }

                if (verbose) std::cout << "create r from rhoa/k" << std::endl;
                data.set("r", data("rhoa") / data("k"));

            } else {
                if (data.haveData("u") && data.allNonZero("i")){
                    if (verbose) std::cout << "create r from u/i" << std::endl;
                    data.set("r", data("u") / data("i"));
                } else {
                    std::cerr << "cannot set r from u/i" << std::endl;
                }
            }
        }

        if (data.haveData("r")){
            if (data.allNonZero("k")){
                if (verbose) std::cout << "add topography effect 'c' k_flat/kCorrect" << std::endl;
                data.set("c", data("k")/kCorrect);
                haveTopoEffect = true;
            }
            data.set("k", kCorrect);

            if (verbose) std::cout << "set new k = k(correctfile)" << std::endl;

            data.set("rhoa", data("r") * kCorrect);

            if (verbose) std::cout << "set new rhoa from r * k(correctfile)" << std::endl;

        } else {
            std::cerr << "no valid ohmic impedance found data file" << std::endl;
            return EXIT_FAILURE;
        }
    } //** end if CORRECT_FILE

    bool fillSelectedError = false;
    if ((needsRhoaErrK && !data.allNonZero("err")) && !estimateError) {
//         if (verbose) std::cout << data.exists("err")<< " " << data.allNonZero("err")<< std::endl;
        // save(data("err"), "err.vec"); //** who needs this?
        if (verbose) std::cout << "Error needed but not found." << std::endl;
        estimateError = true;
        fillSelectedError = true;
    }

    //** estimate input error;
    if (estimateError) {
        if (verbose) std::cout << "Estimate error: (" << fillSelectedError << ") " << errPerc << "% + " << errVolt << std::endl;

        RVector impedance(data.size());
        RVector voltage(data.size());

        //** get voltage from datafile;
        if (data.haveData("u")){
            if (verbose) std::cout << " ... use u for error estimation" << std::endl;
            voltage = data("u");
        } else {
            //** get impedance from datafile
            if (data.haveData("r")) {
                if (verbose) std::cout << " ... use data('r') as impedance." << std::endl;

                impedance = data("r");

            } else if (data.haveData("rhoa")) {
                if (verbose) std::cout << " ... use data('rhoa') for impedance estimation" << std::endl;

                if (!data.allNonZero("k")) {
                    if (verbose) std::cout << "... calculate analytical geometric factors" << std::endl;
                    data.set("k", geometricFactors(data));
                }

                impedance = data("rhoa") / data("k");
            } else {
                std::cerr << "Cannot determine u for error estimation" << std::endl;
                return EXIT_FAILURE;
            }

            data.set("r", impedance);

            //** get voltage from impedance
            if (data.haveData("i")) {
                if (verbose) std::cout << " ... use impedance and measured current." << std::endl;
                voltage = impedance * data("i");
            } else {
                if (verbose) std::cout << " ... use impedance and default current." << defaultCurrent << " A" << std::endl;
                voltage = impedance * defaultCurrent;
            }
            data.set("u", voltage);
        }
        if (fillSelectedError){
            IndexArray errZero(find(data("err") <= 0.0));
            data("err").setVal(abs(RVector(errVolt / voltage)(errZero)) + errPerc / 100.0, errZero);
        } else {
            data.set("err", abs(RVector(errVolt / voltage)) + errPerc / 100.0);
        }
    } // if estimate error

     /*! check whether valid fields are present */
    bool containsI = false, containsU = false, containsIP = false;
    bool containsK = false, containsR = false, containsRhoA = false;
    if (data.haveData("i")) containsI    = true;
    if (data.haveData("u")) containsU    = true;
    if (data.haveData("r")) containsR    = true;
    if (data.haveData("rhoa")) containsRhoA = true;
    if (data.haveData("k"))  containsK    = true;
    if (data.haveData("ip")) containsIP   = true;

    //*** noisify the data if necessary;
    if (noisify) {
        if (verbose) std::cout << "Noisify data" << std::endl;
        RVector rand(data.size());
        randn(rand);

//         std::cout << min(data("rhoa")) << " " << max(data("rhoa"))<< std::endl;
        if (!data.haveData("r")){
            if (!data.haveData("i")){
                data.set("i", RVector(data.size(), 1.0));
            }

            data.set("r", data("u") / data("i"));
        }
//         std::cout << min(rand) << " " << max(rand)<< std::endl;
//         std::cout << min(data("err")) << " " << max(data("err"))<< std::endl;
//         std::cout << min(data("r")) << " " << max(data("r"))<< std::endl;
        data.set("r", data("r") * ((rand * data("err")) + 1.0));
//         std::cout << min(data("r")) << " " << max(data("r"))<< std::endl;

        if (containsRhoA) {
            data.set("rhoa", data("r") * data("k"));
        }
//         std::cout << min(data("rhoa")) << " " << max(data("rhoa"))<< std::endl;
    }

    /*! fill up necessary fields if not present */
    if (!containsR && containsI && containsU)    {
        data.set("r", data("u") / data("i"));
        containsR = true;
    }
    if (!containsK) {
        /*calculate z=0 k factors here for forceFlatEarth*/
        if (verbose) std::cout << "Don't have geometric factors ... calculate them (force flat earth = " << str(forceFlatEarth) << ")" << std::endl;
        data.set("k", geometricFactors(data, 3, forceFlatEarth));
        containsK = true;
    }
    if (!containsRhoA && containsR && containsK) {
        data.set("rhoa", data("r") * data("k"));
        containsRhoA = true;
    }

    /*! Some magic heuristics here  */
    // convention: if median is below -std, then probably all data have negative sign;
    if (containsRhoA && median(data("rhoa")) < stdDev(data("rhoa")) * -0.2) data.set("rhoa", data("rhoa") * -1.0);
    // convention positive (resistivity) phase
    if (containsIP && median(data("ip")) < stdDev(data("ip")) * -0.2) data.set("ip", data("ip") * -1.0);

    if (forceAbsRhoA) data.set("rhoa", abs(data("rhoa")));

    /*! statistics before filtering */
    if (showStat) {
        std::cout << "load: nData=" << data.size() << std::endl;
        std::cout << "rhoa: min = "   << min(data("rhoa"))  << " max = " << max(data("rhoa"))
                  << " mean = "       << mean(data("rhoa")) << " std = " << stdDev(data("rhoa")) << std::endl;
        std::cout << "err: min = "  << min(data("err"))     << " max = " << max(data("err"))
                      << " mean = "   << mean(data("err"))  << " std = " << stdDev(data("err")) << std::endl;

        if (containsK) {
            std::cout << "k: min = "  << min(data("k"))     << " max = " << max(data("k"))
                      << " mean = "   << mean(data("k"))    << " std = " << stdDev(data("k")) << std::endl;
        }

        if (containsIP) {
            std::cout << "ip: min = " << min(data("ip"))    << " max = " << max(data("ip"))
                      << " mean = "   << mean(data("ip"))   << " std = " << stdDev(data("ip")) << std::endl;
        }
    }

    if (!noFilter){

        /*! remove invalid data (nan, inf negative current */
//         if (containsRhoA) data.markValid(find(~(data("rhoa") > 0.0)    | isInfNaN(data("rhoa"))), false);
        if (containsI)    data.markValid(find(~(data("i") > 0.0)       | isInfNaN(data("i"))), false) ;
        if (containsU)    data.markValid(find(~(abs(data("u")) > 0.0)  | isInfNaN(data("u"))), false);
        if (containsIP)   data.markValid(find(~(abs(data("ip")) > 0.0) | isInfNaN(data("ip"))), false);



        /*! remove values within specified bounds */
        if (trimMeanRhoA) {
            THROW_TO_IMPL
/*            rMin = exp(mean(log(data("rhoa"))) - 3.0 * stdDev(log(data("rhoa"))));
            rMax = exp(mean(log(data("rhoa"))) + 3.0 * stdDev(log(data("rhoa"))));
            if (verbose) std::cout << "trim mean: " << rMin  << " " << rMax << std::endl;*/
        }

        //** applying filter rules
        for (size_t i = 0; i < filterRules.size(); i ++){
            std::vector < std::string > token(split(filterRules[ i ], '<'));
            int OP;
            if (token.size() > 1) {
                OP = '<';
            } else {
                token = split(filterRules[ i ], '>');

                if (token.size() > 1) {
                    OP = '>';
                } else {
                    token = split(filterRules[ i ], '=');

                    if (token.size() > 1) {
                        OP = '=';
                    } else {
                        std::cerr << "Cannot parse filter rule: " << filterRules[ i ]  << std::endl;
                        continue;
                    }
                }
            }
            RVector vars;
            double val = 0.0;

            std::string dataString(token[ 0 ]);

            if (data.haveData(dataString)){
                vars = data(dataString);
                if (verbose) std::cout << "Filter: ";

                if (split(token[ 1 ], ':').size() == 1){

                    val = toDouble(split(token[ 1 ], ':')[ 0 ]);
                    if (verbose) std::cout << dataString ;
                    if (verbose) std::cout << " min: " << min(vars) << " max: " << max(vars)<< std::endl;

                } else if (split(token[1], ':').size() == 2){

                    val = toDouble(split(token[ 1 ], ':')[ 0 ]);

                    if (split(token[ 1 ], ':')[ 1 ] == "A"){
                        if (verbose) std::cout << "|" << dataString << "|";
                        vars = abs(vars);
                        if (verbose) std::cout << min(vars) << " " << max(vars)<< std::endl;
                    } else {
                        std::cerr << "Cannot understand filter rule value extension: " << filterRules[ i ]  << std::endl;
                    }
                } else {
                    std::cerr << "Cannot parse filter rule (value-part): " << filterRules[ i ]  << std::endl;
                }

                IndexArray idx;
                switch (OP){
                case '<':
                    if (verbose) std::cout << " < ";
                    idx = find(vars < val);
                    break;
                case '>':
                    if (verbose) std::cout << " > ";
                    idx = find(vars > val);
                    break;
                case '=':
                    if (verbose) std::cout << " = ";
                    idx = find(vars == val);
                    break;
                }
                if (idx.size() > 0) {
                    if (verbose) std::cout << val << " wiping: " << idx.size() << std::endl;
                    data.markValid(idx, false);
                }
            } else {
                std::cout << "No data found for filter rule: " << filterRules[ i ]  << std::endl;
            }
        }
    }

    /*! downweight (keep data) or delete it according to -D */
    if (downWeight) {
        IndexArray notValid(find(data("valid") == 0));

        double rMean = exp(mean(log(data("rhoa")(find(data("rhoa") > 0)))));
        if (verbose) std::cout << "downWeight with rhoaMean = " << rMean << std::endl;

        data("err").setVal(1e10, notValid);
        data("rhoa").setVal(rMean, notValid);
        data.markValid(notValid, true);
    } else {
        if (!noFilter) data.removeInvalid();
    }

    if (deleteUnusedElecs) data.removeUnusedSensors(verbose);

    if (averageDups) data.averageDuplicateData(verbose);

    /*! statistics after filtering */
    if (showStat) {
        std::cout << "save: nData=" << data.size() << std::endl;
        std::cout << "rhoa: min = " << min(data("rhoa")) << " max = " << max(data("rhoa"))
                  << " mean = " << mean(data("rhoa")) << " std = " << stdDev(data("rhoa"))   << std::endl;
        std::cout << "err: min = " << min(data("err")) * 100.0 << "% max = "
                                   << max(data("err")) * 100.0 << "%" << std::endl;
        if (containsK) {
            std::cout << "k: min = " << min(data("k")) << " max = " << max(data("k")) << std::endl;
        }
        if (containsIP) {
            std::cout << "ip: min = " << min(data("ip"))  << " max = " << max(data("ip"))
                      << " mean = "   << mean(data("ip")) << " std = " << stdDev(data("ip"))   << std::endl;
        }
    }

    if (outputFileFormatElecs == NOT_DEFINED)
        outputFileFormatElecs = data.formatStringSensors();

    if (outputFileFormat      == NOT_DEFINED){
        outputFileFormat = data.inputFormatString();

        if (addGeometricFactor && outputFileFormat.rfind("k") == std::string::npos)
            outputFileFormat = outputFileFormat + " k";

        if (estimateError && outputFileFormat.rfind("err") == std::string::npos)
            outputFileFormat = outputFileFormat + " err";

        if (needsRhoaErrK && outputFileFormat.rfind("rhoa") == std::string::npos)
            outputFileFormat = outputFileFormat + " rhoa";

        if (haveTopoEffect) outputFileFormat = outputFileFormat + " c";
    }

    if (sortX){
        if (verbose){
            std::cout << "Sorting sensor positions ..." << std::endl;
        }
        data.sortSensorsX();
    }

    if (sortData){
        if (verbose){
            std::cout << "Sorting data ..." << std::endl;
        }
        data.sortSensorsIndex();
    }



    data.save(outputFileName, outputFileFormat, outputFileFormatElecs, noFilter, verbose);

    if (verbose && estimateError) {
        if (verbose) std::cout << "data min = " << min(data("rhoa")) << " max = " << max(data("rhoa")) << std::endl;
        if (verbose) std::cout << "data error: min = " << min(data("err")) * 100 << "%"
            << " max = " << max(data("err")) * 100 << "%" << std::endl;
    }

    return EXIT_SUCCESS;
}
