#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pygimli as pg
import pybert as pb

print(pg.versionStr())
print(pb.versionStr())
data = pb.DataContainerERT()

data.createSensor(pg.RVector3(0.0, 0.0, 0.0))
print(data)
