# -*- coding: utf-8 -*-
"""
These are the python bindings for libbert

We recommend:

import pybert as pb

"""
from __future__ import print_function

import os
import sys

if sys.platform == 'win32':
    os.environ['PATH'] =  __path__[0] + ';' + os.environ['PATH']

import locale
#print(locale.localeconv()['decimal_point'])

if locale.localeconv()['decimal_point'] == ',':
    print("Found locale decimal_point ',', change it to: decimal point '.':",
          end=' ')
    locale.localeconv()['decimal_point']
    locale.setlocale(locale.LC_NUMERIC, 'C')

try:
    import pygimli as pg
except ImportError as e:
    print(e)
    import traceback
    traceback.print_exc(file=sys.stdout)
    sys.stderr.write("ERROR: cannot import pygimli'.\n")

from pybert.manager import Resistivity, ERTManager, simulate
from pybert.data import showData
from pybert.data import showData as show
from pybert.data import plotERTData
from pybert.data import createData
from pybert.importer import exportData, importData
from pybert.importer import importData as load
from pybert.sip import SIPdata as FDIPdata
from pybert.tdip import TDIPdata

# inject print function for DataContainerERT
def Data_str(self):
    return "Data: Electrodes: " + str(self.sensorCount()) + " data: " + str(
        self.size())
pg.DataContainerERT.__str__ = Data_str

# Versioning using versioneer
from ._version import get_versions

__version__ = get_versions()['version']
del get_versions

def version():
    """Shortcut to show and return current version."""
    pg.info('pybert: ' + __version__ + " pygimli:" + pg.__version__)
    return __version__

#########################
# here could additional functions go keep compatibility

DCSRMultiElectrodeModelling = pg.DCSRMultiElectrodeModelling
DCMultiElectrodeModelling = pg.DCMultiElectrodeModelling
DataContainerERT = pg.DataContainerERT
DataMap = pg.DataMap
DCParaDepth = pg.DCParaDepth
geometricFactor = pg.geometricFactor # DEPRECATED
geometricFactors = pg.geometricFactors
coverageDCtrans = pg.coverageDCtrans
