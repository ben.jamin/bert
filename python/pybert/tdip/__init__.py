#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Time-domain induced polarization (TDIP) Data Manager."""

from .tdipdata import TDIPdata

__all__ = ['TDIPdata']
