# Boundless Electrical Resistivity Tomography (BERT)

Build status: [![Build Status](http://g4.geo.uni-bonn.de:8080/buildStatus/icon?job=pyBERT)](http://g4.geo.uni-bonn.de:8080/job/pyBERT/)

BERT is a software package for modelling and inversion of ERT data.
It has originally been programmed as C++ apps based on the pyGIMLi core library,
plus bash scripts for command line, but is increasingly using Python through
pyGIMLi and pybert, not only for visualization but also for computing.
You may take a look at our 
[Tutorial](http://www.resistivity.net/download/bert-tutorial.pdf) first to learn
the way of doing inversions BERT v2. BERT v3 will be entirely in Python.

See also the pyGIMLi webpage
[www.pygimli.org](http://www.pygimli.org)

## Installation with conda [![Anaconda-Server Badge](https://anaconda.org/gimli/pybert/badges/installer/conda.svg)](https://conda.anaconda.org/gimli) [![Anaconda-Server Badge](https://anaconda.org/gimli/pybert/badges/downloads.svg)](https://anaconda.org/gimli/pybert)[![Anaconda-Server Badge](https://anaconda.org/gimli/pybert/badges/version.svg)](https://anaconda.org/gimli/pybert)[![Anaconda-Server Badge](https://anaconda.org/gimli/pybert/badges/latest_release_date.svg)](https://anaconda.org/gimli/pybert)

On Linux platforms, the most comfortable way to install bert is via the conda
package manager contained in the [Anaconda
distribution](https://docs.continuum.io/anaconda/install#linux-install).
Anaconda is a scientific Python distribution with more than 100 Python packages
included (~400 Mb). You can also use the lightweight alternative
[Miniconda](http://conda.pydata.org/miniconda.html) (~35 Mb) and only install
the packages you like to use. Notes on how to install Miniconda (without root
privileges) can be found
[here](https://gitlab.com/resistivity-net/bert/wikis/install-miniconda).
pybert packages are available for Python 3.5 and 3.6.

```bash
# Add gimli and conda-forge channel (you only need to do this once)
conda config --add channels gimli --add channels conda-forge

# Install pybert (and all dependencies such as pygimli, numpy, mpl, tetgen)
conda install -f pybert

# After installation, you can try out the examples for testing
cd ~/miniconda3/share/examples/inversion/2dflat/gallery
bert gallery.cfg all
bert gallery.cfg show

# Update to a newer version
conda update -f pygimli pybert
```

## Windows binary installers

Download the latest binary installer for a 64bit Windows (7, 8, 10):
[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.7%20bert2.2.9-green.svg)](http://www.resistivity.net/download/setup-bert2.2.9win64py37.exe)
[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.6%20bert2.2.9-green.svg)](http://www.resistivity.net/download/setup-bert2.2.9win64py36.exe)
It works with any Python 3.7.x or 3.6.x (choose the right one!), we recommend the [Anaconda Distribution](https://www.anaconda.com/download/).

Please read the [Tutorial](http://www.resistivity.net/download/bert-tutorial.pdf), particularly what's written in Appendix A for Windows users:
The path to BERT and to Python must be known to the system by either the System Control (Extended Options) or in the command line of the MSYS shell (download the x86_64 variant from https://msys2.github.io) that is needed to run BERT.
Assume BERT is installed in `C:\Software\BERT` and Anaconda in `C:\Software\Anaconda`
```bash
export PATH=$PATH:/c/Software/BERT:/c/Software/Anaconda
bert version  # should yield the BERT version
```
Additionally, the BERT directory must be in the search Path for Python modules to find pyGIMLi:
```bash
export PYTHONPATH=$PYTHONPATH:/c/Software/BERT
python -c "import pygimli;print(pygimli.version())" # should yield the pyGIMLi version
```

To do this for every start copy the lines in your `.bashrc` file:
```bash
echo 'export PATH=$PATH:/c/Software/BERT:/c/Software/Anaconda' >> $HOME/.bashrc
echo 'export PYTHONPATH=$PYTHONPATH:/c/Software/BERT' >> $HOME/.bashrc
```

### Older versions (also for other Python versions)
[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.6%20bert2.2.8-green.svg)](http://www.resistivity.net/download/setup-bert2.2.8win64py36.exe)
[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.6%20bert2.2.7-green.svg)](http://www.resistivity.net/download/setup-bert2.2.7win64py36.exe)
[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.6%20bert2.2.6-green.svg)](http://www.resistivity.net/download/setup-bert2.2.6win64py36.exe)
[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.6%20bert2.2.5-green.svg)](http://www.resistivity.net/download/setup-bert2.2.5win64py36.exe)
[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.5%20bert2.2.4-green.svg)](http://www.resistivity.net/download/setup-bert2.2.4win64py35.exe)
[![Win-Install Badge](https://img.shields.io/badge/Windows%2064bit%20Installer-py3.4%20bert2.1.1-green.svg)](http://www.resistivity.net/download/setup-bert2.1.1win64py34.exe)

## Compiling from source

You need a working [GIMLi installation](http://www.pygimli.org/installation.html) first.
Then create your BERT target path preferable at the same level as your GIMLi path.

```bash
mkdir bert
cd bert
```

Clone your local copy of BERT using your GitLab account.

```bash
git clone https://gitlab.com/resistivity-net/bert.git
```

Create a build path.

```bash
mkdir build
cd build
```

Configure your build.

```bash
cmake ../bert
```

The -G option can be used to specify cmake options, e.g. for Windows

```bash
cmake -G "MSYS Makefiles"
```

You might have to specify the directories where GIMLI they are not found automatically.

```bash
cmake ../bert -DGIMLI_SRC=../../gimli/gimli
```

Or you can specify the path to the library and the header files separately.

```bash
cmake ../bert -DGIMLI_LIBRARIES=/path/to/libgimli.so -DGIMLI_INCLUDE_DIR=/path/to/gimli/src
```

After cmake finds everything, compile the BERT applications (dcmod, dcinv and dcedit).

```bash
make
```

Note that there is no core C++ library anymore since June 2017 when the core moved to pygimli.
If you need the polyTools and mesh generation stuff (recommended) run additionally:

```bash
make bert1
```

Until we fixed some issues in the automatic installation, you can simple add
the bert paths user setting variables.

```bash
export PATH=$PATH:PATH_TO_YOUR_BERTROOT/build/bin
export PYTHONPATH=$PYTHONPATH:PATH_TO_YOUR_BERTROOT/bert/python
```

Some further hints, troubleshooting or additional cmake commands are the same
as for your GIMLi installation and can be found here
<http://pygimli.org/installation.html>.

## Update your installation

To update your installation go into your BERT source repository.

```bash
cd bert/bert
git pull
```

Then you need to rebuild BERT.

    cd ../build
    make
    make bert1

If something goes wrong try to clean your build directory and repeat from the
cmake command above.

# The unified data format

All projects in our working group use a unified format such that data are 
transpararent and transportable. There are a lot of import and export filters 
for third party software or hardware, but internally the following structure 
is used. Since the most applications use multi-electrode systems they are based 
on the way they are addressed (and computed in modeling).

Every data set consists of two mandatory parts: electrode definitions and 
arrangement definition. Subsequently the number of electrodes, 
their positions (x z for 2D, x y z for 3D), the number of data and their 
definitions are given. Comments may be places after the # character. 

After the number of data the definition for each datum is given row-wise 
by the electrode numbers for the current electrodes A and B (C1 and C2) and the 
potential electrodes M and N (P1 and P2). Please not all electrode number starting
from one. Electrodes with a zero number will be treated as infinity electrodes.
They may be followed by other attributes (see list below). 
Standard is the apparent resistivity and optional the relative error as 
exemplified for a tiny dipole-dipole survey.

```
6# Number of electrodes
# x z position for each electrode
0     0
1     0
2     0 # loose ground
3     0
4     0
5     0
6# Number of data
# a b m n rhoa
1   2   3   4  231.2
2   3   4   5  256.7
3   4   5   6  312.8
1   2   4   5  12.1 # possibly an outlier
2   3   5   6  199.7
1   2   5   6  246.2
```

If other fields or order are used, a token string in the line after the data 
number specifies the given fields. The token may be followed by a slash and
a physical unit. The following tokens are allowed (case insensitive).
Tokens	Meaning	possible units

* `a` `c1` electrode number for A (C1)
* `b` `c2` electrode number for B (C2)
* `m` `p1`	electrode number for M (P1)
* `n` `p2`	electrode number for N (P2)
* `rhoa` `Ra`	apparent resistivity	Ohmmeter
* `rho` `r`	Resistance	Ohm
* `err`	relative measurement error in %/100 (default)
* `ip` IP measure	mRad(default)
* `ipErr` absolute IP measure error in mRad(default)
* `i` `I` Current	A(default),mA,uA
* `u` `U` Voltage	V(default),mV,uV

The following sample contains voltages, currents and a percentage error.

```
...
6# Number of data
# a b m n u i/mA err/% 
  1   2   3   4  -0.5305165 102.2 2.4
  2   3   4   5  -0.5305165 99.9  1.4
  3   4   5   6  -0.5305165 95.6  2.6
  1   2   4   5  -0.1326291 100.1 7.6
  2   3   5   6  -0.1326291 80.2 8.6
  1   2   5   6  -0.05305165 77.3 7.5
```

Topography may be given in two ways.

1. Each electrode can be given a height value as z position (preferred by webinv).
2. Alternatively, the data may be followed by a topo list (preferred by DC2dInvRes).

```
...
  1   2   5   6  -0.05305165  7.5
4# Number of topo points
# x h for each topo point
0 353.2
12 357.1
19 359.9
24.5 350
```
