%% load topo and make mesh grid for interp2
topo=load('topo.xyz');
x=unique(topo(:,1));
y=unique(topo(:,2));
[tf,ix]=ismember(topo(:,1),x);
[tf,iy]=ismember(topo(:,2),y);
[X,Y]=meshgrid(x,y);
Z=zeros(length(y),length(x));
for i=1:length(ix), Z(iy(i),ix(i))=topo(i,3); end
%% draw a line across the dam
dam=[49 213;-300 512];
dam(1,:)=dam(1,:)+diff(dam)*0.032;
dam(2,:)=dam(2,:)+diff(dam)*0.062;
%% create linearly spaced x/y vectors
xi=linspace(dam(1,1),dam(2,1),30);
yi=linspace(dam(1,2),dam(2,2),30);
%% interpolate the elevation
zi=interp2(X,Y,Z,xi,yi);zi([1 end])
foot=round([xi(:) yi(:) zi(:)]*10)/10;
foot=flipud(foot);
save foot.xyz foot -ascii
%% determine a 2d profile for later use
li=[0 cumsum(sqrt(diff(xi).^2+diff(yi).^2))];
lz=[li(:) zi(:)];
save foot2d.xz lz -ascii
%% find intersection points of topo
outline=[];zw=164.5; % water level
for i=1:length(x),
   fi=find(diff(sign(Z(:,i)-zw)));
   for l=1:length(fi),
      yy=y(fi(l):fi(l)+1);
      w1=abs((Z(fi(l),i)-zw)/diff(Z(fi(l):fi(l)+1,i)));
      yyy=[1-w1 w1]*y(fi(l):fi(l)+1);
      if (x(i)>-320)&&(yyy<1200)&&(yyy>222), outline=[outline;x(i) yyy]; end
   end
end
outline=[outline;foot([1 end],1:2)];
%%
li=size(outline,1)-1;
while length(li)<size(outline,1),
   po=outline;po(li(1:end-1),:)=Inf; 
   di=sqrt((po(:,1)-po(li(end),1)).^2+(po(:,2)-po(li(end),2)).^2);
   di(li(end))=Inf;
   [midi,li(end+1)]=min(di);
end
outline=outline(li,:);
%% ?????
outline(find(li==length(li))+1:end,:)=[];
outline(find((outline(:,1)==-121)&(outline(:,2)==716.1)),:)=[];
% plot(outline(li,1),outline(li,2),'-.');axis equal;grid on
outline(:,3)=zw;
outline=round(outline*10)/10;
%b�ser Punkt wo sich alles kreuzt
fi1=find((round(outline(:,1))==-121)&(round(outline(:,2))==716))
outline(fi1,:)=[];
%% remove doubled points
[size(outline,1) size(unique(outline,'rows'),1)]
outline(find(sum(diff(outline).^2,2)==0),:)=[];
[size(outline,1) size(unique(outline,'rows'),1)]
%%
outline=flipud(outline);
save outline.xyz outline -ascii
%%
all=[outline;foot(2:end,:)];
save poly.xyz all -ascii
% plot(all(:,1),all(:,2),'x-');

