#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pygimli as g
from pygimli.meshtools import merge2Meshes
import pylab as P

def myextrap(x, xp, yp):
    """np.interp function with linear extrapolation"""
    y = P.interp(x, xp, yp)
    y = P.where(x < xp[0], yp[0]+(x-xp[0])*(yp[0]-yp[1])/(xp[0]-xp[1]), y)
    y = P.where(x > xp[-1], yp[-1]+(x-xp[-1])*(yp[-1]-yp[-2])/(xp[-1]-xp[-2]), y)
    return y

profileFileName = 'alldata.pro'
with open(profileFileName, 'r') as fi:
    content = fi.readlines()
fi.close()

mesh3d = g.Mesh(2)
RES = P.array([])

for line in content:    
    datfile = line.split()[0]

    #read in map file containing tape, x,y,z and 2d-x
    posfile = 'pos/' + datfile.replace('.dat','.txyz')
    t,x,y,z,x2d = P.loadtxt(posfile).T

    vtkfile = datfile.replace('.dat','.vtk')
    mesh = g.Mesh('2d/' + vtkfile)

    xm = g.x(mesh.positions())
    zn = g.y(mesh.positions()) # in 2d z becomes y in vtk

    xn = myextrap(xm, x2d, x)
    yn = myextrap(xm, x2d, y)

    for i in range(mesh.nodeCount()):
        mesh.node(i).setPos(g.RVector3(xn[i], yn[i], zn[i]))

    mesh3d = merge2Meshes(mesh3d, mesh)
    mesh.exportVTK('3d/' + vtkfile)
    RES = P.hstack((RES, mesh.exportData('Resistivity/Ohmm')))

All.addExportData('Resistivity/Ohmm', g.asvector(RES))
print(mesh3d, len(RES))
All.exportVTK('3d/' + profile.replace('.pro','_2d.vtk'))
