python << END
import pygimli as pg
import numpy as np

dx = 2.
nb = 1
xmin, xmax = 0., 20.
ymin, ymax = 0., 32.5
zmax = 10.

x = pg.asvector(np.arange(xmin-dx*nb,xmax+dx*(nb+1), dx))
y = pg.asvector(np.arange(ymin-dx*nb,ymax+dx*(nb+1), dx))
z = pg.asvector(np.arange(-np.ceil( zmax / dx ), 1.) * dx) 
print x, y, z

mesh = pg.Mesh()
mesh.create3DGrid(x,y,z)
for i,c in enumerate(mesh.cells()):
    c.setMarker(i+1)

mesh2 = pg.Mesh()
mesh2 = mesh.createH2()
mesh2.createNeighbourInfos()
bounds=[]
for b in mesh2.boundaries():
    if b.leftCell() is None or b.rightCell() is None and b.center()[2] < 0.:
        bounds.append(b)
        b.setMarker(1)

print(mesh)
print(mesh2)
len(bounds)
mesh2.save('para')
mesh2.exportVTK('para')
poly = pg.Mesh()
poly.createMeshByBoundaries(mesh2, mesh2.findBoundaryByMarker(1));
poly.exportAsTetgenPolyFile('paraBoundary')
END
polyConvert -V paraBoundary
SIZE=300
polyCreateWorld -x $SIZE -y $SIZE -z $SIZE world
polyScale -z 0.5 world
polyMerge world paraBoundary worldSurface
polyConvert -V worldSurface
polyAddVIP -x0 -y0 -z0 -H worldSurface
tetgen143 -pazVAC worldSurface
meshconvert -vBDM -it -o worldBoundary worldSurface.1
python << END
import pygimli as pg
worldBoundary = pg.Mesh('worldBoundary.bms')
worldPoly = pg.Mesh()
worldPoly.createMeshByBoundaries( worldBoundary, worldBoundary.findBoundaryByMarker( -2, 0 ) );
worldPoly.exportAsTetgenPolyFile( "worldBoundary.poly" )
END
polyConvert -V worldBoundary
# merge world and para without check
polyMerge -N worldBoundary paraBoundary allBoundary
polyConvert -V allBoundary
# remove doubled Nodes by recounting
python << END
import pygimli as pg
Poly = pg.Mesh()
with open('allBoundary.poly','r') as f:
	line1 = f.readline()
	nnodes = int(line1.split()[0])
	nodes=[]
	for i in range(nnodes):
		pos = f.readline().split()
		p = pg.RVector3(float(pos[1]), float(pos[2]), float(pos[3]))
		n = Poly.createNodeWithCheck(p)
		n.setMarker(int(pos[4]))
		nodes.append(n)
		
	line2 = f.readline()
	nfaces = int(line2.split()[0])
	for i in range(nfaces):
		bla = f.readline().split()
		ind = f.readline().split()
		fa = Poly.createTriangleFace(nodes[int(ind[1])],nodes[int(ind[2])],
		                             nodes[int(ind[3])],0)
		fa.setMarker(int(bla[2]))  # keep boundary conditions

		print(Poly)
Poly.exportAsTetgenPolyFile('test.poly')
END
polyConvert -V test
rm -f wronpg.vtk right.vtk test.1.*
tetgen143 -d test
if [ -f test.1.face ]
then
    echo "Detected intersecting subfaces! See wronpg.vtk"
    meshconvert -V -it -o wrong test.1
    return 1
else
    tetgen -pazVACY -q 1.2 test
    meshconvert -VBDM -it -o right test.1
    rm -rf test.1.*
fi
python << END
import pygimli as pg
# read inner and outer mesh
Outer = pg.Mesh('right.bms')
Inner = pg.Mesh('para.bms')
print(Outer)
print(Inner)
# set all outer cells to 0
for c in Outer.cells(): c.setMarker(0)
# set all inner cells to 1 (inversion on tetrahedra, comment for hexahedra)
#for c in Inner.cells(): c.setMarker(1)
# merge inner mesh into outer
for c in Inner.cells():
        nodes = pg.stdVectorNodes( )
        for i, n in enumerate( c.nodes() ):
            nodes.append(Outer.createNodeWithCheck(n.pos()))
                
        Outer.createCell(nodes, c.marker());
        
print(Outer)
Outer.save('mesh')
Outer.exportVTK('mesh')
END