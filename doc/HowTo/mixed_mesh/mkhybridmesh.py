import pygimli as pg
import numpy as np
from pygimli.viewer import showMesh
from pygimli.meshtools import merge2Meshes

filename = 'ostland3.ohm'
data = pg.DataContainer( filename, 'a b m n' )
print data

plc = pg.Mesh(2)
# create unit vectors in x and z direction
ex, ez = pg.RVector3(1.0, 0.0, 0.0), pg.RVector3(0.0, 1.0, 0.0)
pbou, bou = 10., 400.  # parameter boundary and overall bounday
xe, ze = pg.x(data.sensorPositions()), pg.z(data.sensorPositions())
n0 = plc.createNode(xe[0] - pbou, ze[0], 0.0)  # left edge of paradomain
nold = n0
elnodes = []
for i in range(data.sensorCount()):  # along the electrodes
    n = plc.createNode(xe[i], ze[i], 0.0, -99)
    elnodes.append(n)
    if i > 0:  # intermediate node (PARADX=0.5)
        nmid = plc.createNode((xe[i-1]+xe[i]) / 2, (ze[i-1]+ze[i])/2, 0., 0)
        plc.createEdge(nold, nmid, -1)
        plc.createEdge(nmid, n, -1)
    else:
        plc.createEdge(nold, n, -1)

    nold = n

n1 = plc.createNode(n.pos() + ex * pbou)  # right edge of paradomain
plc.createEdge(n, n1, -1)
nold = n1
n = plc.createNode(n1.pos() + ex * bou)  # upper right edge of domain
plc.createEdge(nold, n, -1)
nold = n
n = plc.createNode(n1.pos() + ex * bou - ez * bou)  # lower right edge
plc.createEdge(nold, n, -2)
nold = n
n = plc.createNode(n0.pos() - ex * bou - ez * bou)  # lower left edge
plc.createEdge(nold, n, -2)
nold = n
n = plc.createNode(n0.pos() - ex * bou)  # upper left edge
plc.createEdge(nold, n, -2)
plc.createEdge(n, n0, -1)
plc.createEdge(n0, elnodes[0], -1)
plc.createEdge(elnodes[-1], n1, -1)

z1, z2, dz = -12, -60, 4
# npts = 40 #n1.x()-n0.x() / dz
# xp = np.linspace( n0.x(), n1.x(), npts )
xp = np.arange(n0.x(), n1.x(), dz)
zp = np.arange(z2, z1+dz, dz)
grid = pg.createGrid(xp, zp)
for b in grid.boundaries():
    if b.rightCell() is None or b.leftCell() is None:  # outer boundary
        b.setMarker(0)  # prevent it from becoming a structural boundary
        plc.copyBoundary(b)

# connect topography (left+right point) with outer rectangle of grid
plc.createEdge(n0, plc.createNodeWithCheck(pg.RVector3(xp[0], zp[-1], 0)))
plc.createEdge(n1, plc.createNodeWithCheck(pg.RVector3(xp[-1], zp[-1], 0)))

# put two region markers (inside,outside) in and mesh using triangle
tri = pg.TriangleWrapper(plc)
tri.addRegionMarkerTmp(1, n0.pos() - ex*0.1 - ez*0.1, 0.0)
tri.addRegionMarkerTmp(2, n0.pos() + ex*0.1 - ez*0.1, 0.0)
tri.setSwitches('-pzeAfaq34')
#%%
mesh = pg.Mesh(2)
tri.generate(mesh)
print(mesh)
mesh2 = pg.Mesh(2)
mesh2.createMeshByMarker(mesh, 1, 3)  # without cells in box (marker=0)
print(mesh2)
# showMesh(mesh2)
#%%
for c in grid.cells(): c.setMarker(2)
#%%
mesh3 = merge2Meshes(mesh2, grid)
print(mesh3)
#%%
bm = mesh3.boundaryMarker()
#%%
mesh3.save('mymesh.bms')
mesh3.exportVTK('mymesh.vtk')
showMesh(mesh3)
#%%
#showMesh(mesh3,np.asarray(mesh3.cellMarker())+.1)
