#
# GIMLI_FOUND
# GIMLI_INCLUDE_DIRS
# GIMLI_LIBRARIES

# Optional user supplied search path.
set (GIMLI_PREFIX_PATH "" CACHE PATH "Directory to search for GIMLI header and library files")

# Find include directory.
find_path (GIMLI_INCLUDE_DIR gimli.h 
    ${GIMLI_PREFIX_PATH}
    ${GIMLI_SRC}/src
    ${PROJECT_SOURCE_DIR}/../../gimli/trunk/src
    ${PROJECT_SOURCE_DIR}/../../gimli/gimli/src
    ${PROJECT_SOURCE_DIR}/../../libgimli/trunk/src
    /usr/local/include
    /usr/includes
)

# With Win32, important to have both
IF(WIN32)
  FIND_LIBRARY(GIMLI_LIBRARIES gimli
                ${PROJECT_SOURCE_DIR}/../../gimli/build/bin
                ${PROJECT_SOURCE_DIR}/../../libgimli/build/bin
                ${GIMLI_BUILD}/bin
                /usr/local/lib
                /usr/lib)
                message( "${PROJECT_SOURCE_DIR}/../../gimli/build/bin " ${GIMLI_LIBRARIES})
ELSE(WIN32)
  # On unix system, debug and release have the same name
  FIND_LIBRARY(GIMLI_LIBRARIES gimli
                ${GIMLI_PREFIX_PATH}
                ${GIMLI_BUILD}/bin
                ${PROJECT_SOURCE_DIR}/../gimli/build/lib
                ${PROJECT_SOURCE_DIR}/../../gimli/build/lib
                ${PROJECT_SOURCE_DIR}/../libgimli/build/lib
                ${PROJECT_SOURCE_DIR}/../../libgimli/build/lib
               /usr/local/lib
               /usr/lib)
ENDIF(WIN32)

# Standard package handling
include(FindPackageHandleStandardArgs)
    find_package_handle_standard_args(GIMLI "Gimli could not be found." GIMLI_LIBRARIES GIMLI_INCLUDE_DIR)

mark_as_advanced(
  GIMLI_INCLUDE_DIR
  GIMLI_LIBRARIES
  )
