// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#include "meshadds.h"
#include "line.h"

int createCircle( Domain2D & mesh, const RealPos & pos, double radiusA, double radiusB, int segments, double offset, int direction, bool edges, int marker, double dx ){
  
  double x = 0, y = 0, z = 0, dphi = 0, phi = ( offset * ( 2 *PI_ ) ) / 360. ;
  dphi = 2 *PI_ / ( segments );
  
  Node * nodea = NULL, * nodeb = NULL;

  for (int i = 0; i < segments; i++){
    x = radiusA * cos( phi );
    z = radiusB * sin( phi );

    if ( fabs( x ) < 1E-6 ) x = 0.0;
    if ( fabs( z ) < 1E-6 ) z = 0.0;

    nodea = mesh.createNode( x + pos.x(), z + pos.z(), 0.0, mesh.nodeCount(), marker);

    if ( i > 1 && edges ) {
      mesh.createEdge( * nodea, * nodeb, marker );
    }

    phi = phi + direction * dphi;
    nodeb = nodea;
  }
  if ( edges ){
    mesh.createEdge( mesh.node( mesh.nodeCount() - segments ), 
		     mesh.node(mesh.nodeCount() - segments + 1), marker );
    mesh.createEdge( * nodeb, mesh.node( mesh.nodeCount() - segments ), marker );
  }

  if ( marker != 0 ) mesh.createRegionMarker( pos, marker, dx );
  return 1;
}

Polygon findAllNodesBetween( Mesh2D & mesh, Node * pNodeStart, Node * pNodeEnd, int marker ){
  map < double, Node * > nodeMap;

  nodeMap[ 0.0 ] = pNodeStart;
  nodeMap[ pNodeStart->distanceTo( *pNodeEnd ) ] = pNodeEnd;

  Line line( pNodeStart->pos(), pNodeEnd->pos() );
  
  for ( int i = 0, imax = mesh.nodeCount(); i < imax; i ++){
    if ( marker == -1 ){
      if ( line.touch( mesh.node( i ).pos() ) == 3 ){
	nodeMap[ pNodeStart->distanceTo( mesh.node( i ) ) ] = &mesh.node( i );
      }
    } else if ( mesh.node( i ).marker() == marker ){
      nodeMap[ pNodeStart->distanceTo( mesh.node( i ) ) ] = &mesh.node( i );
    }
  }
  
  Polygon poly;
  for ( map< double, Node * >::iterator it = nodeMap.begin(); it != nodeMap.end(); it ++){
    poly.push_back( it->second );
  }
  return poly;
}

void createEdges( Mesh2D & mesh, const Polygon & line, int marker ){
  for ( size_t i = 0; i < line.size() -1; i ++ ){
    mesh.createEdge( *line[ i ], *line[ i + 1 ], mesh.boundaryCount(),  marker );
  }
}

vector < RealPos > tapeMeasureToCartesian( const vector < RealPos > & tpm, const RealPos & cartOffset ){
  //** check if tapeMeasure startCoord is valid
  if ( tpm[ 0 ][ 0 ] != 0.0 ){
    cerr << WHERE_AM_I << " tapeMeasure starting point is invalid " << tpm[ 0 ] << endl;
  } 

  //** check if tapeMeasureCoords. incremental
  for ( uint i = 1; i < tpm.size(); i ++ ){
    if ( tpm[ i ][ 0 ] < tpm[ i - 1 ][ 0 ] ){
      cerr << WHERE_AM_I << " tapeMeasure data are not incremental" << endl;
    } 
  }

  vector < RealPos > cart;
  cart.push_back( tpm[ 0 ] + cartOffset );

  double dist = tpm[ 0 ].x();
  double dz = tpm[ 0 ].y();
  
  for ( uint i = 1; i < tpm.size(); i++ ){
    dist = tpm[ i ].x() - tpm[ i - 1 ].x();
    dz = tpm[ i ].y() - tpm[ i - 1 ].y();
    cart.push_back( cart[ i - 1 ] + RealPos( sqrt( dist * dist - dz * dz ),  dz, 0.0 ) );
  }
  return cart;
}

vector < RealPos > cartesianToTapeMeasure( const vector < RealPos > &cart ){
  vector < RealPos > tpm;
  tpm.push_back( RealPos( 0.0, cart[ 0 ].y(), 0.0 ) );

  for ( uint i = 1; i < cart.size(); i ++ ){
    tpm.push_back( RealPos( tpm[ i - 1 ].x() + cart[ i ].distance( cart[ i - 1 ] ), cart[ i ].y(), 0.0 ) );
  }

  return tpm;
}

/*
$Log: meshadds.cpp,v $
Revision 1.4  2005/10/14 15:31:37  carsten
*** empty log message ***

Revision 1.3  2005/07/01 15:11:28  carsten
*** empty log message ***

Revision 1.2  2005/03/20 22:51:59  carsten
*** empty log message ***

Revision 1.1  2005/01/04 19:22:30  carsten
add in cvs

*/
