// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#ifndef LONGOPTIONS__H
#define LONGOPTIONS__H LONGOPTIONS__H

#include <list>
#include <string>
#include <iostream>


/*
  Apple (OS X) and Sun systems declare getopt in unistd.h,
  other systems (Linux) use getopt.h
*/
#if defined ( __APPLE__ ) || ( defined (__SVR4) && defined (__sun) )
#include <unistd.h>
struct option {
# if defined __STDC__ && __STDC__
  const char *name;
# else
  char *name;
# endif
  /* has_arg can't be an enum because some compilers complain about
     type mismatches in all the code that assumes it is an int.  */
  int has_arg;
  int *flag;
  int val;
};
#else
#include <getopt.h>
#endif

using namespace std;
#include "dcfemlib.h"


//!
/*! */
class LongOption {
public:
  /*!   
    hasArg: no_argument, required_argument or optional_argument
  */
  LongOption( const string & name, int hasArg, char key, const string & help )
  : name_( name ), hasArg_( hasArg ), key_( key ), help_( help ){}
  /*! */
  ~LongOption( ){}

  /*! */
  string name() const { return name_; }
  /*! */
  int key() const { return key_; }
  /*! */
  string help() const { return help_; }
  /*! */
  int hasArg() const { return hasArg_; }

protected:
  string name_;
  int hasArg_;
  int *flag_;
  int key_;
  string help_;
};

//!
/*! */
class DLLEXPORT LongOptionsList : public list< class LongOption >{
public:
  /*! */
  LongOptionsList(){
    opts_ = new( struct option[ 1 ] );
  }
  ~LongOptionsList(){ 
    delete [] opts_;
  }
  /*! */
  void insert( const string & name, int hasArg, char key, const string & help );
  /*! */
  void printHelp( const string & main );
  /*! */
  struct option * long_options();

  /*! */
  void setLastArg( const string & lastArg ){ lastArg_ = lastArg; }
  /*! */
  string lastArg( ) const { return lastArg_; }

  /*! */
  void setDescription( const string & description ){ description_ = description; }
  /*! */
  string description( ) const { return description_; }

protected:
  string lastArg_;
  string description_;
  struct option * opts_;
};

#endif //LONGOPTIONS__H


/*
$Log: longoptions.h,v $
Revision 1.6  2005/10/17 15:10:17  carsten
*** empty log message ***

Revision 1.5  2005/03/15 18:01:39  carsten
*** empty log message ***

Revision 1.4  2005/03/15 18:01:03  carsten
*** empty log message ***

Revision 1.3  2005/03/15 17:50:58  carsten
*** empty log message ***

Revision 1.2  2005/01/06 13:20:07  carsten
*** empty log message ***

Revision 1.1  2005/01/04 19:22:30  carsten
add in cvs


*/

