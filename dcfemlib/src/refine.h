// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#ifndef REFINE__H
#define REFINE__H REFINE__H

#include "dcfemlib.h"
using namespace DCFEMLib;

#include "elements.h"
#include "basemesh.h"
using namespace MyMesh;

#include "myvec/vector.h"
using namespace MyVec;

class SparcepNodeMatrix {
public:
  SparcepNodeMatrix( size_t size );
  ~SparcepNodeMatrix( );
  void setVal( size_t i, size_t j, Node * node );
  
  Node * val( size_t i, size_t j );

  void save( const string & fname);

protected:
  Node* **mat;
  size_t size_;
};

int markerT( Node * n0, Node * n1 );

RVector estimateZienkiewiczZhuError( const BaseMesh & mesh, const RVector & u );

#endif // REFINE__H

/*
$Log: refine.h,v $
Revision 1.4  2006/10/30 12:50:09  carsten
*** empty log message ***

Revision 1.3  2006/09/25 11:31:49  carsten
*** empty log message ***

Revision 1.2  2005/10/14 15:31:37  carsten
*** empty log message ***

Revision 1.1  2005/03/10 16:04:38  carsten
*** empty log message ***

*/
