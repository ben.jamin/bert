/***************************************************************************
 *   Copyright (C) 2007 by the resistivity.net development team            *
 *   Carsten R�cker carsten@resistivity.net                                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef _DCFEMLIB_CHOLMODWRAPPER__H
#define _DCFEMLIB_CHOLMODWRAPPER__H

#include "dcfemlib.h"
#include "myvec/vector.h"
#include "myvec/matrix.h"

#include "solver.h"

struct cholmod_sparse;
struct cholmod_factor;
struct cholmod_common;

namespace DCFEMLib{

class DLLEXPORT CHOLMODWrapper : public SolverWrapper {
public:
    CHOLMODWrapper( RDirectMatrix & S, bool verbose = false );
    virtual ~CHOLMODWrapper();
 
    static bool valid();

    int factorise();
    int solve( const RVector & rhs, RVector & solution );

protected:
    int initialize_( RDirectMatrix & S );

    cholmod_common *c_;
    cholmod_sparse *A_;
    cholmod_factor *L_;
  
    int * AcolPtr_;
    int * ArowIdx_;
    double * Avals_;
};

} //namespace DCFEMLib;

#endif // _DCFEMLIB_CHOLMODWRAPPER__H


/*
$Log: cholmodWrapper.h,v $
Revision 1.1  2008/05/19 14:52:28  carsten
Add cholmodwrapper, interpolate V.2 in createSurface


*/
