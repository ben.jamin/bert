// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//


#include "interpolate.h"
#include "line.h"
#include "plane.h"

#include "mesh2d.h"
#include "mesh3d.h"

#include "stlmatrix.h"
using namespace MyVec;

#include "solver.h"

#include <cmath>
using namespace std;

#define tolDirectMatch 1e-6
#define tolLine 1e-5
#define tolPlane 1e-6
#define tolTet 1e-6

#ifdef HAVE_ANN
#include <ANN/ANN.h>			// ANN declarations
#endif //HAVE_ANN

#include "setalgorithm.h"

KDTree::KDTree( const BaseMesh & mesh ){
  //  cout << "Build kd-tree" << endl;
  bool nodes_ = true;

  dimension_ = mesh.dim();
  if ( nodes_ ){
    maxPts_ = mesh.nodeCount() + 1;
  } else {
    maxPts_ = mesh.cellCount() + 1;
  }
  nPts_ = 0;

#ifdef HAVE_ANN
//   ANNpointArray dataPts;
//   dataPts = annAllocPts( maxPts_, dimension_ ); // allocate data points

//  dataPts_ = new ANNpointArray( );
  dataPts_ = annAllocPts( maxPts_, dimension_ ); // allocate data points

  RealPos inputPos( 0.0, 0.0, 0.0 );

  for ( int i = 0; i < maxPts_ -1; i ++ ) {

    if ( nodes_ ){
      inputPos = mesh.node( i ).pos();
    } else {
      inputPos = mesh.cell( i ).averagePos();
    }

    for ( int j = 0; j < dimension_; j ++ ) {
      static_cast<ANNpointArray>(dataPts_)[ i ][ j ] = inputPos[ j ];
    }
    //       dataPts_[ i ][ 0 ] = mesh.node( i ).x();
    //       dataPts_[ i ][ 1 ] = mesh.node( i ).y();
    //       if ( dimension_ == 3 ) dataPts_[ i ][ 2 ] = mesh.node( i ).z();
    nPts_++;
  }
  //  cout << "collect Nodes" << endl;
  tree_ = new ANNkd_tree( static_cast<ANNpointArray>(dataPts_),		// the data points
			  nPts_,             // number of points
			  dimension_ );	// dimension of space
  //  cout << "finish" << endl;

#else //HAVE_ANN
  cerr << WHERE_AM_I << " ANN not installed" << endl;
#endif //HAVE_ANN
}
KDTree::~KDTree(){
#ifdef HAVE_ANN
  delete tree_;
  annDeallocPts( dataPts_ );
#else //HAVE_ANN
  cerr << WHERE_AM_I << " ANN not installed" << endl;
#endif //HAVE_ANN
}

void KDTree::search( const RealPos & queryPos, int neightN,
		     vector < int > & nn_idx, vector < double > & dists ) const {
#ifdef HAVE_ANN
  nn_idx.clear();
  dists.clear();

  ANNidx annIdx[ neightN ];			// allocate near neigh indices
  ANNdist annDists[ neightN ];			// allocate near neighbor dists

  ANNpoint queryPt = annAllocPt( dimension_ );
  for ( int i = 0; i < dimension_; i ++ ) queryPt[ i ] = queryPos[ i ];

  static_cast< ANNkd_tree* >( tree_ )->annkSearch( queryPt,		// query point
		     neightN,		// number of near neighbors
		     annIdx,		// nearest neighbors (returned)
		     annDists,		// distance (returned)
		     0.0);			// error bound

  for ( int i = 0; i < neightN; i ++ ){
    nn_idx.push_back( annIdx[ i ] );
    dists.push_back( annDists[ i ] );
  }
#else //HAVE_ANN
  cerr << WHERE_AM_I << " ANN not installed" << endl;
#endif //HAVE_ANN
}

MeshEntity * findMeshEntity( const KDTree & tree, const BaseMesh & mesh, const RealPos  & queryPos ){

  MeshEntity * entity = NULL;
  int neightN = 1;

  vector < int > nn_idx;
  vector < double > dists;
  SetpCells cellsAtNode, allreadyChecked;

  //while ( neightN < floor(mesh.nodeCount() / 10.0) ){
  //  while ( neightN < floor(mesh.nodeCount() / 10000.0) ){
  while ( neightN < 1000.0 && neightN < mesh.nodeCount()  ) {

    tree.search( queryPos, neightN, nn_idx, dists );

    if ( sqrt( dists[ 0 ] ) < tolDirectMatch && neightN == 1 ){
      return &mesh.node( nn_idx[ 0 ] );
    }

    for ( int j = (int)( neightN / 2.0 ); j < neightN; j ++ ){
      cellsAtNode = mesh.node( nn_idx[ j ] ).cellSet();// + mesh.node( nn_idx[ j ] ).boundarySet();
//       cout<< j << " " << mesh.node( nn_idx[ j ] ) << endl;
//       show( cellsAtNode );
//       show( mesh.node( nn_idx[ j ] ).cellSet() );
//       show( mesh.node( nn_idx[ j ] ).boundarySet() );

      cellsAtNode = diff( cellsAtNode, allreadyChecked );

      for ( SetpCells::iterator it = cellsAtNode.begin(); it != cellsAtNode.end(); it ++ ){
	if ( (*it)->touch( queryPos, 1e-8, false) ) return *it;
        //if ( (entity = isWithin( queryPos, *(*it), false )) != NULL ) return entity;
      }
      allreadyChecked = allreadyChecked + cellsAtNode;
    }
    neightN *= 2;
  }

  cerr << WHERE_AM_I << " brute force search. ";

  //** start brute force search
  for ( int i = 0; i < mesh.cellCount(); i ++ ){
    if ( mesh.cell( i ).touch( queryPos, 1e-8, true ) ) return &mesh.cell( i );
  }

  BaseElement * nearest = & mesh.findNearestCell( queryPos );
  cout << "nearest: " << nearest->rtti() << "  " << nearest->id() << endl;

  cout << isWithin( queryPos, *nearest, true ) << endl;
  TO_IMPL;

  //  exit(0);

  vector < int > cellIdx;
//   cellsAtNode.clear();
//   neightN = 1;
//   for ( int k = 0; k < neightN; k ++ ){
//     cout << k << " " << queryPos << mesh.node( nn_idx[ k ] ).pos()
// 	 << queryPos.distance( mesh.node( nn_idx[ k ] ).pos() ) << endl;
//     cellsAtNode = cellsAtNode + mesh.node( nn_idx[ k ] ).cellSet();
//   }

//   for ( SetpCells::iterator it = cellsAtNode.begin(); it != cellsAtNode.end(); it ++ ){
//     cellIdx.push_back( (*it)->id() );
//     if ( mesh.dim() == 2 ){
//       isWithin( queryPos, dynamic_cast<Triangle &>(*(*it)), true );
//     } else {
//       // isWithin( queryPos, dynamic_cast<Tetrahedron &>(*(*it)), true );
//     }
//   }

  BaseMesh *testMesh = NULL;

  switch ( mesh.dim() ){
  case 2:
    testMesh = new Mesh2D(); break;
  case 3:
    testMesh = new Mesh3D();    break;
  }

  cellIdx.clear(); cellIdx.push_back( nearest->id() );
  testMesh->findSubMeshByIdx( mesh, cellIdx );
  testMesh->showInfos();
  testMesh->createNode( queryPos, testMesh->nodeCount(), -99 );

  char strdummy[1024];
  sprintf( strdummy, "queryFail" );
  testMesh->saveVTKUnstructured( (string)strdummy );
  delete testMesh;
  cerr << WHERE_AM_I << " aborting" << endl;
  exit(1);
}

vector < MeshEntity * > findMeshEntities( const BaseMesh & mesh, const vector < RealPos > & queryVector, bool verbose ){
  int nQuerys = queryVector.size();

  vector < MeshEntity * > entities;

  KDTree tree( mesh );

  if ( verbose ) cout << "Start interpolate: " << endl;

  int vcountmax = (long)rint( nQuerys / 1000);
  for ( int i = 0, vcount = 0; i < nQuerys; i++, vcount ++ ){
    if ( verbose ){
      if ( vcount == vcountmax ){
	cout << "\r" << ceil( ( i * 1000.0 ) / ( nQuerys ) ) / 10 << " % ";
	vcount = 0;
      }
    }

    entities.push_back( findMeshEntity( tree, mesh, queryVector[ i ] ) );
  }
  if ( verbose ) cout << endl;

  return entities;
}

vector < Node * > findMeshEntityOld( const KDTree & tree, const BaseMesh & mesh, const RealPos  & queryPos ){
  vector < Node * > samplingNodes;

  int neightN = 1;

  vector < int > nn_idx;
  vector < double > dists;

  SetpCells cellsAtNode, allreadyChecked;

  //while ( neightN < floor(mesh.nodeCount() / 10.0) ){
  //  while ( neightN < floor(mesh.nodeCount() / 10000.0) ){
  while ( neightN < 1000.0 && neightN < mesh.nodeCount()  ) {

    tree.search( queryPos, neightN, nn_idx, dists );

    if ( sqrt( dists[ 0 ] ) < tolDirectMatch && neightN == 1 ){
      samplingNodes.push_back( &mesh.node( nn_idx[ 0 ] ) );
      return samplingNodes;
    }

    for ( int j = (int)( neightN / 2.0 ); j < neightN; j ++ ){
      cellsAtNode = mesh.node( nn_idx[ j ] ).cellSet();
      cellsAtNode = diff( cellsAtNode, allreadyChecked );

      for ( SetpCells::iterator it = cellsAtNode.begin(); it != cellsAtNode.end(); it ++ ){

	if ( mesh.dim() == 2 ){
	  samplingNodes = isWithin( queryPos, dynamic_cast<Triangle &>(*(*it)), false );
	} else {
	  samplingNodes = isWithin( queryPos, dynamic_cast<Tetrahedron &>(*(*it)), false );
	}
	if ( samplingNodes.size() > 0 ){
	  return samplingNodes;
	}
      }
      allreadyChecked = allreadyChecked + cellsAtNode;
    }
    neightN *= 2;
  }

  cerr << "brute force search. ";

  //** start brute force search
  for ( int i = 0; i < mesh.cellCount(); i ++ ){
    if ( mesh.dim() == 2 ){
      samplingNodes = isWithin( queryPos, dynamic_cast<Triangle &>( mesh.cell( i ) ), false );
    } else{
      samplingNodes = isWithin( queryPos, dynamic_cast<Tetrahedron &>( mesh.cell( i ) ), false );
    }
    if ( samplingNodes.size() > 0 ){
      //      nearest = & mesh.cell( i ); break;
      cerr << " ." << endl;
      return samplingNodes;
    }
  }
//   //**
//   for ( int i = 0; i < mesh.cellCount(); i ++ ){
//     if ( queryPos.isWithin( dynamic_cast< Triangle & >( mesh.cell( i ) ), 1e-8 ) ){
//       nearest = & mesh.cell( i );
//       break;
//     }
//   }

  BaseElement * nearest = & mesh.findNearestCell( queryPos );
  cout << "nearest: " << nearest->rtti() << "  " << nearest->id() << endl;

  switch( nearest->rtti() ){
  case MYMESH_TRIANGLE_RTTI:
    cout << "Check triangle manual." << endl;
    cout << queryPos.isWithin( dynamic_cast<Triangle &>( *nearest ), tolPlane, true ) << endl;
    cout << isWithin( queryPos, dynamic_cast<Triangle &>( *nearest ), true ).size() << endl;
    break;
    case MYMESH_TETRAHEDRON_RTTI:
    case MYMESH_TETRAHEDRON10_RTTI:
    cout << "Check tetrahedron manual." << endl;
    cout << queryPos.isWithin( dynamic_cast<Tetrahedron &>( *nearest ), tolTet, true ) << endl;
    cout << isWithin( queryPos,  dynamic_cast<Tetrahedron &>( *nearest ), true ).size() << endl;
    samplingNodes.push_back( &nearest->node(0) );
    samplingNodes.push_back( &nearest->node(1) );
    samplingNodes.push_back( &nearest->node(2) );
    samplingNodes.push_back( &nearest->node(3) );
    return samplingNodes;
    break;
  default:
    TO_IMPL;
  }

  //  exit(0);

  vector < int > cellIdx;
//   cellsAtNode.clear();
//   neightN = 1;
//   for ( int k = 0; k < neightN; k ++ ){
//     cout << k << " " << queryPos << mesh.node( nn_idx[ k ] ).pos()
// 	 << queryPos.distance( mesh.node( nn_idx[ k ] ).pos() ) << endl;
//     cellsAtNode = cellsAtNode + mesh.node( nn_idx[ k ] ).cellSet();
//   }

//   for ( SetpCells::iterator it = cellsAtNode.begin(); it != cellsAtNode.end(); it ++ ){
//     cellIdx.push_back( (*it)->id() );
//     if ( mesh.dim() == 2 ){
//       isWithin( queryPos, dynamic_cast<Triangle &>(*(*it)), true );
//     } else {
//       // isWithin( queryPos, dynamic_cast<Tetrahedron &>(*(*it)), true );
//     }
//   }

  BaseMesh *testMesh = NULL;

  switch ( mesh.dim() ){
  case 2:
    testMesh = new Mesh2D(); break;
  case 3:
    testMesh = new Mesh3D();    break;
  }

  cellIdx.clear(); cellIdx.push_back( nearest->id() );
  testMesh->findSubMeshByIdx( mesh, cellIdx );
  testMesh->showInfos();
  testMesh->createNode( queryPos, testMesh->nodeCount(), -99 );

  char strdummy[1024];
  sprintf( strdummy, "queryFail" );
  testMesh->saveVTKUnstructured( (string)strdummy );
  delete testMesh;
  cerr << WHERE_AM_I << " aborting" << endl;
  exit(1);
}

vector < Node * > findMeshEntityMid( const KDTree & tree, const BaseMesh & mesh, const RealPos  & queryPos ){
 TO_IMPL
  vector < Node * > samplingNodes;
    return samplingNodes;
//   int neightN = 1;

//   vector < int > nn_idx;
//   vector < double > dists;

//   SetpCells cellsAtNode, allreadyChecked;

//   while ( neightN < ( mesh.nodeCount() / 10.0 ) ){

//     tree.search( queryPos, neightN, nn_idx, dists );

//     for ( int j = (int)( neightN / 2.0 ); j < neightN; j ++ ){
//       cout << j << queryPos << " " << mesh.cell( nn_idx[ j ] ).averagePos() << dists[ j ] << endl;
//       if ( mesh.dim() == 2 ){
// 	samplingNodes = isWithin( queryPos, dynamic_cast<Triangle &>( mesh.cell( nn_idx[ j ] ) ), false );
//       } else {
// 	//	  samplingNodes = isWithin( queryPos, dynamic_cast<Tetrahedron &>(*(*it)), false );
// 	TO_IMPL;
//       }
//       if ( samplingNodes.size() > 0 ){
// 	return samplingNodes;
//       }
//     }
//     neightN *= 2;
//   }

//   cerr << "hier sollte ich nie sein." << endl;
//   BaseElement * nearest = & mesh.cell( nn_idx[ 0 ] );

// //   //** start brute force search
// //   for ( int i = 0; i < mesh.cellCount(); i ++ ){
// //     if ( mesh.dim() == 2 ){
// //       samplingNodes = isWithin( queryPos, dynamic_cast<Triangle &>( mesh.cell( i ) ), false );
// //     }
// //     if ( samplingNodes.size() > 0 ){
// //       //      nearest = & mesh.cell( i ); break;
// //       return samplingNodes;
// //     }
// //   }
// //   //**
// //   for ( int i = 0; i < mesh.cellCount(); i ++ ){
// //     if ( queryPos.isWithin( dynamic_cast< Triangle & >( mesh.cell( i ) ), 1e-8 ) ){
// //       nearest = & mesh.cell( i );
// //       break;
// //     }
// //   }


//   cout << "nearest: " << nearest->rtti() << "  " << nearest->id() << endl;

//   switch( nearest->rtti() ){
//   case MYMESH_TRIANGLE_RTTI:
//     cout << "Check triangle manual." << endl;
//     cout << queryPos.isWithin( dynamic_cast<Triangle &>( *nearest ), 1e-8, true ) << endl;
//     break;
//   default:
//     TO_IMPL;
//   }

//   //  exit(0);

//   vector < int > cellIdx;
// //   cellsAtNode.clear();
// //   neightN = 1;
// //   for ( int k = 0; k < neightN; k ++ ){
// //     cout << k << " " << queryPos << mesh.node( nn_idx[ k ] ).pos()
// // 	 << queryPos.distance( mesh.node( nn_idx[ k ] ).pos() ) << endl;
// //     cellsAtNode = cellsAtNode + mesh.node( nn_idx[ k ] ).cellSet();
// //   }

// //   for ( SetpCells::iterator it = cellsAtNode.begin(); it != cellsAtNode.end(); it ++ ){
// //     cellIdx.push_back( (*it)->id() );
// //     if ( mesh.dim() == 2 ){
// //       isWithin( queryPos, dynamic_cast<Triangle &>(*(*it)), true );
// //     } else {
// //       // isWithin( queryPos, dynamic_cast<Tetrahedron &>(*(*it)), true );
// //     }
// //   }

//   BaseMesh *testMesh = NULL;

//   switch ( mesh.dim() ){
//   case 2:
//     testMesh = new Mesh2D(); break;
//   case 3:
//     testMesh = new Mesh3D();    break;
//   }

//   cellIdx.clear(); cellIdx.push_back( nearest->id() );
//   testMesh->findSubMeshByIdx( mesh, cellIdx );
//   testMesh->showInfos();
//   testMesh->createNode( queryPos, testMesh->nodeCount(), -99 );

//   char strdummy[1024];
//   sprintf( strdummy, "queryFail" );
//   testMesh->saveVTKUnstructured( (string)strdummy );
//   delete testMesh;
//   cerr << WHERE_AM_I << " aborting" << endl;
//   exit(1);
 }

vector < vector < Node * > > findMeshEntitiesOld( const BaseMesh & mesh, const vector < RealPos > & queryVector, bool verbose ){
  int nQuerys = queryVector.size();

  vector < vector < Node * > > vectorSamplingNodes;

  KDTree tree( mesh );

  if ( verbose ) cout << "Start interpolate: " << endl;

  int vcountmax = (long)rint( nQuerys / 1000);
  for ( int i = 0, vcount = 0; i < nQuerys; i++, vcount ++ ){
    if ( verbose ){
      if ( vcount == vcountmax ){
	cout << "\r" << ceil( ( i * 1000.0 ) / ( nQuerys ) ) / 10 << " % ";
	vcount = 0;
      }
    }

   vectorSamplingNodes.push_back( findMeshEntityOld( tree, mesh, queryVector[ i ] ) );
  }
  if ( verbose ) cout << endl;

  return vectorSamplingNodes;
}

// void interpolate( const BaseMesh & inMesh, const vector < vector < RVector > > & data,
// 		  const vector < vector < Node * > > & vectorSamplingNodes, vector < vector < RVector > > & queryData ){
// TO_IMPL
// }

// void interpolate( const BaseMesh & inMesh, const vector < RVector > & data,
// 		  const vector < vector < Node * > > & vectorSamplingNodes, vector < RVector > & queryData ){

//   int nData = data[ 0 ].size();
//   if ( inMesh.nodeCount() != nDdata ){
//     cout << WHERE_AM_I << " to few data " << inMesh.nodeCount()  << "!=" << nData << endl;
//   }

//   int nQueryData = vectorSamplingNodes.size();
//   if ( vectorSamplingNodes.size() != nQueryData ){
//     cout << WHERE_AM_I << " to few sampling data-vectors " << vectorSamplingNodes.size()  << "!=" << nQueryData << endl;
//   }
//}

void interpolate( const vector < MeshEntity * > & vecEntities, const RVector & data,
		  const vector < RealPos > & queryVector, RVector & queryData ){
  vector < RVector > dataVec, queryDataVec;
  dataVec.push_back( data );
  interpolate( vecEntities, dataVec, queryVector, queryDataVec );
  queryData = queryDataVec[ 0 ];
}

void interpolate( const vector < MeshEntity * > & vecEntities, const vector < RVector > & data,
		  const vector < RealPos > & queryVector, vector < RVector > & queryData ){
  TO_IMPL
}

void interpolate( const vector < vector < Node * > > & vectorSamplingNodes, const RVector & data,
		  const vector < RealPos > & queryVector, RVector & queryData ){
  vector < RVector > dataVec, queryDataVec;
  dataVec.push_back( data );
  interpolate( vectorSamplingNodes, dataVec, queryVector, queryDataVec );
  queryData = queryDataVec[ 0 ];
}

void interpolate( const vector < vector < Node * > > & vectorSamplingNodes, const vector < RVector > & data,
		  const vector < RealPos > & queryVector, vector < RVector > & queryData ){

  int nVecs = data.size();
  int nQueryData = queryVector.size();
  queryData.clear();
  for ( int i = 0; i < nVecs; i ++ ) queryData.push_back( RVector( nQueryData ) );

  for ( int i = 0; i < nQueryData; i ++ ){
    interpolateValues( vectorSamplingNodes[ i ], data, queryVector[ i ], queryData, i );
  }
}

void interpolateValues( const vector < Node * > & vectorSamplingNodes, const vector < RVector > & data,
			const RealPos & queryPos, vector < RVector > & queryData, int ith ){
  switch( vectorSamplingNodes.size() ){
  case 1: // direct Match;
    for ( uint i = 0; i < data.size(); i ++ ) queryData[ i ][ ith ] = data[ i ][ vectorSamplingNodes[ 0 ]->id() ];
    break;
  case 2: // found Edge;
    interpolateValuesEdge( vectorSamplingNodes, data, queryPos, queryData, ith );
    break;
  case 3: // found Triangle;
    interpolateValuesTri( vectorSamplingNodes, data, queryPos, queryData, ith );
    break;
  case 4: // found Tetrahedron;
    interpolateValuesTet( vectorSamplingNodes, data, queryPos, queryData, ith );
    break;
  default:
    cerr << WHERE_AM_I << " not yet defined " << vectorSamplingNodes.size() << endl;
  }
}

void interpolateValuesEdge( const vector < Node * > & samplingPoints, const vector < RVector > & data,
			    const RealPos & queryPos, vector < RVector > & queryData, int ith){

  double radius = samplingPoints[ 0 ]->pos().distance( samplingPoints[ 1 ]->pos() );
  double rmin = samplingPoints[ 0 ]->pos().distance( queryPos );

  double v0 = 0.0, v1 = 0.0, result = 0.0;

  for ( uint i = 0; i < data.size(); i ++ ) {
    v0 = data[ i ][ samplingPoints[ 0 ]->id() ];
    v1 = data[ i ][ samplingPoints[ 1 ]->id() ];
    result = v0 + ( (v1-v0) / radius ) * rmin;
    queryData[ i ][ ith ] = result;
  }
}

void interpolateValuesTri( const vector < Node * > & samplingPoints, const vector < RVector > & data,
			   const RealPos & queryPos, vector < RVector > & queryData, int ith){

  RealPos planeNormTarget( 0.0, 0.0, 1.0 );
  RealPos p1( samplingPoints[ 0 ]->pos() );
  RealPos p2( samplingPoints[ 1 ]->pos() );
  RealPos p3( samplingPoints[ 2 ]->pos() );
  RealPos planeNorm( Plane( p1, p2, p3 ).norm());
  RealPos queryPosTmp( queryPos );

  //  cout <<p1<< p2<< p3 << planeNorm << endl;
  double phix = 0.0, phiy = 0.0, phiz = 0.0;
  int count = 0;
  while ( Plane( p1, p2, p3 ).norm().round( 1e-11 ) != planeNormTarget &&
	  Plane( p1, p2, p3 ).norm().round( 1e-11 ) != planeNormTarget * -1.0 ){
    count ++;
    planeNorm = Plane( p1, p2, p3 ).norm();

    RealPos ro( planeNorm.cross( planeNormTarget ) );

    if ( ro.abs() > TOLERANCE ){
      phix = asin( ro.x() ), phiy = asin( ro.y() ), phiz = asin( ro.z() );

      p1.rotate( phix, phiy, phiz );
      p2.rotate( phix, phiy, phiz );
      p3.rotate( phix, phiy, phiz );
      queryPosTmp.rotate( phix, phiy, phiz );
    } else {
      break;
    }

    if ( count > 32 ) {
      cerr << WHERE_AM_I << " Hier sollt ich nicht sein" << endl;
      cerr << Plane( p1, p2, p3 ) << endl;
      cerr << Plane( p1, p2, p3 ).norm().round(1e-11) << endl;

      exit(1);
    }
  }

  STLVector u( 3 );
  STLMatrix A( 3, 3 );
  STLVector c( 3 );

  double result = 0.0;

  A[ 0 ][ 0 ] = 1.0;  A[ 0 ][ 1 ] = p1.x(); A[ 0 ][ 2 ] = p1.y();
  A[ 1 ][ 0 ] = 1.0;  A[ 1 ][ 1 ] = p2.x(); A[ 1 ][ 2 ] = p2.y();
  A[ 2 ][ 0 ] = 1.0;  A[ 2 ][ 1 ] = p3.x(); A[ 2 ][ 2 ] = p3.y();

  for ( uint i = 0; i < data.size(); i ++ ) {
    for ( int j = 0; j < 3; j ++ ) u[ j ] = data[ i ][ samplingPoints[ j ]->id() ];
    solveLU( A, c, u );
    result = c[ 0 ] + c[ 1 ] * queryPosTmp.x() + c[ 2 ] * queryPosTmp.y();
    queryData[ i ][ ith ] = result;
  }
}


void interpolateValuesTet( const vector < Node * > & samplingPoints, const vector < RVector > & data,
			   const RealPos & queryPos, vector < RVector > & queryData, int ith){

  STLVector u( 4 );
  STLMatrix A( 4, 4 );
  STLVector c( 4 );

  for ( int i = 0; i < 4; i ++ ){
    A[ i ][ 0 ] = 1.0;
    A[ i ][ 1 ] = samplingPoints[ i ]->x();
    A[ i ][ 2 ] = samplingPoints[ i ]->y();
    A[ i ][ 3 ] = samplingPoints[ i ]->z();
  }

  double result = 0;
  for ( uint i = 0; i < data.size(); i ++ ){
    for ( int j = 0; j < 4; j ++ ) u[ j ] = data[ i ][ samplingPoints[ j ]->id() ];

    solveLU( A, c, u );

    result = c[ 0 ] + c[ 1 ] * queryPos.x() + c[ 2 ] * queryPos.y() + c[ 3 ] * queryPos.z();

    queryData[ i ][ ith ] = result;
  }
}


MeshEntity * isWithin( const RealPos & queryPos, BaseElement & cell, bool verbose ){

  switch( cell.rtti() ){
  case MYMESH_EDGE_RTTI:
  case MYMESH_EDGE3_RTTI:{
    Line line( cell.node(0).pos(), cell.node(1).pos() );
    int touch = line.touch( queryPos, tolLine, verbose );
    switch ( touch ){
    case 2: return & cell.node(0); break;
    case 3: return & cell; break;
    case 4: return & cell.node(1); break;
    }
  } break;
  case MYMESH_TRIANGLE_RTTI:
  case MYMESH_TRIANGLE6_RTTI:
  case MYMESH_TRIANGLEFACE_RTTI:
  case MYMESH_TRIANGLE6FACE_RTTI:
    if ( queryPos.isWithin( dynamic_cast< Triangle &>(cell) ) ) return & cell;
    break;
  case MYMESH_TETRAHEDRON_RTTI:
  case MYMESH_TETRAHEDRON10_RTTI:
    if ( queryPos.isWithin( dynamic_cast< Tetrahedron &>(cell), tolTet, verbose ) ) return & cell;

    break;
  }

  return NULL;
}

vector < Node * > isWithin( const RealPos & queryPos, const Edge & edge, bool verbose ){
  if ( verbose ) cout << "Edge: " << edge.id() << " n0: " << edge.nodeA().id() << " n1: " << edge.nodeB().id() << endl;

  vector < Node * > sampleNodes;
  int within = 0, touch = 0;
  Line line( edge );
  touch = line.touch( queryPos, tolLine, verbose );

  if ( verbose ) {
    cout << "\t tolLine: " << tolLine << endl;
    cout << "\t Line: "<< line.p0() << queryPos << line.p1() << endl
	 << "\t dist1: "<< line.p0().distance( queryPos ) << " dist2: " << line.p1().distance( queryPos ) << endl
	 << "\t\tl: " <<  line.length() << " d: " << line.distance( queryPos )
	 << " touch = " << touch << endl;
  }

  switch ( touch ){
  case 2:
    sampleNodes.push_back( &edge.nodeA() ); return sampleNodes;
    break;
  case 3:
    sampleNodes.push_back( &edge.nodeA() ); sampleNodes.push_back( &edge.nodeB() ); return sampleNodes;
    break;
  case 4:
    sampleNodes.push_back( &edge.nodeB() ); return sampleNodes;
    break;
  }

  if (verbose ) cout << "dont touch " << endl;
  return sampleNodes;
}

vector < Node * > isWithin( const RealPos & queryPos, const Triangle & tri, bool verbose ){
  Node *n0 = &tri.node( 0 ), *n1 = &tri.node( 1 ), *n2 = &tri.node( 2 );

  if ( verbose ) cout << "Tri: " << tri.id() << " n0: " << n0->id()
		      << " n1: " << n1->id() << " n2: " << n2->id() << endl;

  RealPos p0( n0->pos() ), p1( n1->pos() ), p2( n2->pos() );
  Plane plane( p0, p1, p2 );

  vector < Node * > sampleNodes;

  if ( verbose ){
    cout << "\t d:" << plane.distance( queryPos ) <<  " tol: "<< tolPlane << endl;
  }

  if ( !plane.touch( queryPos, tolPlane ) ) {
    if ( verbose ){
      cout << "not on plane " << endl;
    }
    return sampleNodes;
  }

  Edge edge( *n0, *n1 );
  sampleNodes = isWithin( queryPos, edge, verbose );
  if ( sampleNodes.size() > 0 ) return sampleNodes;

  edge.setNodes( *n0, *n2 );
  sampleNodes = isWithin( queryPos, edge, verbose );
  if ( sampleNodes.size() > 0 ) return sampleNodes;

  edge.setNodes( *n1, *n2 );
  sampleNodes = isWithin( queryPos, edge, verbose );
  if ( sampleNodes.size() > 0 ) return sampleNodes;

  if ( queryPos.isWithin( tri ) ){
    sampleNodes.push_back( & tri.node( 0 ) );
    sampleNodes.push_back( & tri.node( 1 ) );
    sampleNodes.push_back( & tri.node( 2 ) );
    return sampleNodes;
  }

  if (verbose ) cout << "dont touch " << endl;
  return sampleNodes;
}

vector < Node * > isWithin( const RealPos & queryPos, const Tetrahedron & tet, bool verbose ){
  vector < Node * > sampleNodes;

  Node *n0 = &tet.node( 0 ), *n1 = &tet.node( 1 ); Node *n2 = &tet.node( 2 ), *n3 = &tet.node( 3 );

  if ( verbose ) cout << "Tet: " << tet.id()
		      << " n0: " << n0->id() << " n1: " << n1->id() << " n2: " << n2->id() << " n3: " << n3->id()
		      << endl;

  Edge edge1( *n0, *n1 );
  sampleNodes = isWithin( queryPos, edge1, verbose );
  if ( sampleNodes.size() > 0 ) return sampleNodes;

  Edge edge2( *n0, *n2 );
  sampleNodes = isWithin( queryPos, edge2, verbose );
  if ( sampleNodes.size() > 0 ) return sampleNodes;

  Edge edge3( *n0, *n3 );
  sampleNodes = isWithin( queryPos, edge3, verbose );
  if ( sampleNodes.size() > 0 ) return sampleNodes;

  Edge edge4( *n1, *n2 );
  sampleNodes = isWithin( queryPos, edge4, verbose );
  if ( sampleNodes.size() > 0 ) return sampleNodes;

  Edge edge5( *n1, *n3 );
  sampleNodes = isWithin( queryPos, edge5, verbose );
  if ( sampleNodes.size() > 0 ) return sampleNodes;

  Edge edge6( *n2, *n3 );
  sampleNodes = isWithin( queryPos, edge6, verbose );
  if ( sampleNodes.size() > 0 ) return sampleNodes;


  Triangle tri1( *n0, *n1, *n2, -1, -1, false );
  sampleNodes = isWithin( queryPos, tri1, verbose );
  if ( sampleNodes.size() > 0 ) return sampleNodes;

  Triangle tri2( *n1, *n2, *n3, -1, -1, false );
  sampleNodes = isWithin( queryPos, tri2, verbose );
  if ( sampleNodes.size() > 0 ) return sampleNodes;

  Triangle tri3( *n0, *n2, *n3, -1, -1, false );
  sampleNodes = isWithin( queryPos, tri3, verbose );
  if ( sampleNodes.size() > 0 ) return sampleNodes;

  Triangle tri4( *n0, *n1, *n3, -1, -1, false );
  sampleNodes = isWithin( queryPos, tri4, verbose );
  if ( sampleNodes.size() > 0 ) return sampleNodes;

  if ( queryPos.isWithin( tet, tolTet, verbose ) ){
    sampleNodes.push_back( & tet.node( 0 ) );
    sampleNodes.push_back( & tet.node( 1 ) );
    sampleNodes.push_back( & tet.node( 2 ) );
    sampleNodes.push_back( & tet.node( 3 ) );
    return sampleNodes;
  }

  if (verbose ) cout << "dont touch " << endl;
  return sampleNodes;
}

std::vector < double > jacTri__;
std::vector < bool > taged__;
std::vector <  std::vector < Triangle* > > neighbors__;
long counter__ = 0;

Triangle * neighbourCellFrom2( Triangle * cell, int id ){
    std::set< BaseElement * > common;
    std::set< BaseElement * > set1( cell->node( (id)%3 ).cellSet() );
    std::set< BaseElement * > set2( cell->node( (id+1)%3 ).cellSet() );
    set_intersection( set1.begin(), set1.end(),
                      set2.begin(), set2.end(), std::inserter( common, common.begin() ) );

  /*for ( std::set< BaseElement * > ::iterator it = set1.begin(); it != set1.end(); it ++ ){
        common.insert( *it );
    }
  */
  //  std::cout << common.size() << std::endl;
    common.erase( cell );
    if ( common.size() == 1 ) {
        return (Triangle*)*common.begin();
    } else {
//         std::cout << "no found" <<std::endl;
//         std::cout << cell->node( id%3 ).cellSet().size() << std::endl;
//         std::cout << cell->node( (id+1)%3).cellSet().size() << std::endl;

        return NULL;
    }
}

void interpolateFrom2( BaseMesh & mesh, const RVector & data, const BaseMesh & meshPos, RVector & iData,
                     bool bruteForce){

    std::cout << "using interpolateFrom2: " << bruteForce<< std::endl;
    jacTri__.resize( mesh.cellCount() );
    taged__.resize( mesh.cellCount() );
    neighbors__.resize( mesh.cellCount(), std::vector < Triangle* >( 3 ) );

    for ( size_t i = 0; i < jacTri__.size(); i ++ ) {
      jacTri__[ i ] = mesh.cell( i ).jacobianDeterminant();
      for ( size_t j = 0; j < 3; j ++ ) {
        neighbors__[ i ][ j ] = neighbourCellFrom2( (Triangle*)(&mesh.cell( i )), j );
      }
    }

    std::vector < RealPos >  pos( meshPos.nodePositions() );

    if ( (size_t)iData.size() != pos.size() ){
      iData.resize( pos.size() );
    }

    std::vector < Triangle * > cells( pos.size() );
    int vcountmax = (long)rint( pos.size() / 1000);
    for ( uint i = 0, vcount = 0; i < pos.size(); i ++, vcount ++ ) {
      if ( (long)vcount == vcountmax ){
        // does not work under windows (segfault)!
        //cout << "\r" << ceil( ( i * 1000.0 ) / ( pos.size() ) ) / 10 << " % ";
        vcount = 0;
      }

      cells[ i ] = meshFindCellFrom2( mesh, pos[ i ], bruteForce );

      if ( cells[ i ] ){
        iData[ i ] = interpolateFrom2( cells[ i ], pos[ i ], data );
      } else {
//                     std::cout << pos[ j ]<< std::endl;
//                     for ( uint i = 0; i < imesh.cellCount(); i ++ ){
//                     	if ( imesh.cell( i ).shape().touch( pos[ j ], true ) ){
//                         	std::cout << imesh.cell( i ) << std::endl;
//                         }
//                     }
                    //exit(0);
      }
    }

    std::cout << "counter__: " << counter__ << std::endl;
}

bool touchFrom2( Triangle * tri, const RealPos & pos, int & pFunIdx ){
    counter__++;
    double x21 = tri->node( 1 ).pos()[ 0 ] - tri->node( 0 ).pos()[ 0 ];
    double x31 = tri->node( 2 ).pos()[ 0 ] - tri->node( 0 ).pos()[ 0 ];
    double y21 = tri->node( 1 ).pos()[ 1 ] - tri->node( 0 ).pos()[ 1 ];
    double y31 = tri->node( 2 ).pos()[ 1 ] - tri->node( 0 ).pos()[ 1 ];
    double xp1 = pos[ 0 ] - tri->node( 0 ).pos()[ 0 ];
    double yp1 = pos[ 1 ] - tri->node( 0 ).pos()[ 1 ];

    //** find pos in isoparametric koordinates
    //** shape functions N1 = 1-r-s; N2 = r; N3 = s;
    //** if max( N1, N2, N3 ) == 1 pos match the respective node
    double r = ( xp1 * y31 - x31 * yp1 ) / jacTri__[ tri->id() ];
    double s = ( x21 * yp1 - xp1 * y21 ) / jacTri__[ tri->id() ];

    double N1 = 1.0 - r - s;
    double N2 = r;
    double N3 = s;

    //** fun < 0 outside; fun == 0 obBound; fun > 0 inside; max(fun ) =barycenter (1/3 for triangle)
    double fun = std::min( std::min( N1, N2 ), N3 );
    if ( N1 == fun ) pFunIdx = 1;
    else if ( N2 == fun ) pFunIdx = 2;
    else if ( N3 == fun ) pFunIdx = 0;

    bool verbose = false;
    if( verbose ){
        std::cout << " IO: " << fun << std::endl;
    	std::cout << N1 << std::endl;
    	std::cout << N2 << std::endl;
    	std::cout << N3 << std::endl;
    }

    if ( std::fabs( fun ) < TOLERANCE  ) return true; //** on boundary
    if ( fun > 0.0 ) return true; //** inside
    //** outside
    return false;
}
void untagall(){ for ( size_t i = 0; i < taged__.size(); i ++ ) taged__[ i ] = false; }

double interpolateFrom2( Triangle * tri, const RealPos & pos, const RVector & data ){
    double x21 = tri->node( 1 ).pos()[ 0 ] - tri->node( 0 ).pos()[ 0 ];
    double x31 = tri->node( 2 ).pos()[ 0 ] - tri->node( 0 ).pos()[ 0 ];
    double y21 = tri->node( 1 ).pos()[ 1 ] - tri->node( 0 ).pos()[ 1 ];
    double y31 = tri->node( 2 ).pos()[ 1 ] - tri->node( 0 ).pos()[ 1 ];
    double xp1 = pos[ 0 ] - tri->node( 0 ).pos()[ 0 ];
    double yp1 = pos[ 1 ] - tri->node( 0 ).pos()[ 1 ];

    double r = ( xp1 * y31 - x31 * yp1 ) / jacTri__[ tri->id() ];
    double s = ( x21 * yp1 - xp1 * y21 ) / jacTri__[ tri->id() ];

    double N1 = 1.0 - r - s;
    double N2 = r;
    double N3 = s;

    return N1 * data[ tri->node( 0 ).id() ] + N2 * data[ tri->node( 1 ).id() ] +
            N3 * data[ tri->node( 2 ).id() ];
}

Triangle * meshFindCellFrom2( BaseMesh & mesh, const RealPos & pos, bool bruteForce ){
    int pFunIdx;
    Triangle * cell = NULL;
    if ( bruteForce ){
        for ( int i = 1; i < mesh.cellCount(); i ++ ) {
            if ( touchFrom2( (Triangle*)(&mesh.cell( i )), pos, pFunIdx ) ){
                cell = (Triangle*)(&mesh.cell( i ));
                break;
            }
        }
    } else {
        untagall();

        for ( int i = 0; i < mesh.cellCount(); i ++ ) {
            cell = (Triangle*)(&mesh.cell( i ));
            do {
                if ( taged__[ cell->id() ] ) {
                    cell = NULL;
                } else {
                    taged__[ cell->id() ] = true;
                    if ( touchFrom2( cell, pos, pFunIdx ) ) {
                        return cell;
                    } else {
                        cell = neighbors__[ cell->id() ][ pFunIdx ];
                    }

                }
            }  while ( cell );
        }
  //      std::cout << " n: " << count;
    }
    return cell;
}

/*
$Log: interpolate.cpp,v $
Revision 1.25  2008/05/27 12:44:29  thomas
no message

Revision 1.24  2008/05/19 14:52:28  carsten
Add cholmodwrapper, interpolate V.2 in createSurface

Revision 1.23  2008/03/14 11:16:25  carsten
*** empty log message ***

Revision 1.22  2008/03/01 23:00:48  carsten
*** empty log message ***

Revision 1.21  2006/10/08 14:11:38  carsten
*** empty log message ***

Revision 1.20  2006/08/28 13:55:47  carsten
*** empty log message ***

Revision 1.19  2006/08/27 22:30:12  carsten
*** empty log message ***

Revision 1.17  2006/08/27 21:52:31  carsten
*** empty log message ***

Revision 1.16  2006/06/06 09:41:49  carsten
*** empty log message ***

Revision 1.15  2006/05/22 14:53:38  carsten
*** empty log message ***

Revision 1.14  2006/04/17 20:58:03  carsten
*** empty log message ***

Revision 1.13  2005/10/24 09:52:08  carsten
*** empty log message ***

Revision 1.12  2005/10/18 12:48:11  carsten
*** empty log message ***

Revision 1.11  2005/10/17 15:10:17  carsten
*** empty log message ***

Revision 1.10  2005/10/17 11:56:58  carsten
*** empty log message ***

Revision 1.9  2005/10/12 12:35:21  thomas
*** empty log message ***

Revision 1.8  2005/09/28 13:07:23  carsten
*** empty log message ***

Revision 1.7  2005/08/12 16:43:20  carsten
*** empty log message ***

Revision 1.6  2005/04/07 11:41:08  carsten
*** empty log message ***

Revision 1.5  2005/03/30 18:37:14  carsten
*** empty log message ***

Revision 1.4  2005/03/17 18:17:13  carsten
*** empty log message ***

Revision 1.3  2005/03/15 16:08:41  carsten
*** empty log message ***

Revision 1.2  2005/01/12 21:06:45  carsten
*** empty log message ***

Revision 1.1  2005/01/12 20:32:40  carsten
*** empty log message ***

*/
