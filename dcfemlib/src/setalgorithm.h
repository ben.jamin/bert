// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#ifndef SETALGORITHM__H
#define SETALGORITHM__H SETALGORITHM__H

template < class Set > Set operator + ( const Set & a, const Set & b ){
  Set tmp( a );
  if ( & a != & b ){
    typename Set::const_iterator it = b.begin();
    while ( it != b.end() ){
      tmp.insert( *it++ );
    }
  }
  return tmp;
}

template < class Set > Set diff( const Set & a, const Set & b ){
  Set tmp( a );
  if ( & a != & b ){
    typename Set::const_iterator it= b.begin();
    while ( it != b.end() ){
      tmp.erase( *it++ );
    }
  }
  return tmp;
}

// template < class Set > Set operator - ( const Set & a, const Set & b ){
//   Set tmp( a );
//   if ( & a != & b ){
//     typename Set::const_iterator it= b.begin();
//     while ( it != b.end() ){
//       tmp.erase( *it++ );
//     }
//   }
//   return tmp;
// }

template < class Set > Set operator & ( const Set & a, const Set & b ){
  Set tmp;
  typename Set::const_iterator it = b.begin();
  for ( ; it != b.end(); it++ ){
    if ( a.find( (*it) ) != a.end() ){
      tmp.insert( *it );
    }
  }
  return tmp;
}

template < class Set > Set operator | ( const Set & a, const Set & b ){
  //  Set tmp( a );
  //   TO_IMPL;
   //   TO_CHECK;
//   typename Set::const_iterator it = b.begin();

//   for ( ; it != b.end(); it++ ){
//      if ( (*a.find( (*it) ) ) == NULL ) tmp.insert( (*it ) );
//   }
//   return tmp;
}

// SetpTriangleFaces operator || ( const SetpTriangleFaces & a,  const SetpTriangleFaces & b){
//   TO_IMPL
//   TO_CHECK
//   SetpTriangleFaces tmp( a );
//   for ( SetpTriangleFaces::const_iterator it = b.begin(); it != b.end(); it++ ){
//     if ( (*a.find( (*it) )) == NULL ) tmp.insert( (*it ) );
//   }
//   return tmp;
// }

// SetpTriangleFaces operator + ( const SetpTriangleFaces & a,  const SetpTriangleFaces & b){
//   SetpTriangleFaces tmp( a );
//   for ( SetpTriangleFaces::const_iterator it = b.begin(); it != b.end(); it++ ) tmp.insert( (*it) );
//   return tmp;
// }
// SetpTriangleFaces operator - ( const SetpTriangleFaces & a,  const SetpTriangleFaces & b){
//   SetpTriangleFaces tmp( a );
//   for ( SetpTriangleFaces::const_iterator it = b.begin(); it != b.end(); it++ ) tmp.erase( (*it) );
//   return tmp;
// }
// SetpTriangleFaces operator && ( const SetpTriangleFaces & a,  const SetpTriangleFaces & b){
//   SetpTriangleFaces tmp;
//   for ( SetpTriangleFaces::const_iterator it = b.begin(); it != b.end(); it++ ){
//     if ( a.find( (*it) ) != a.end() ){
// //       cout << a.size() << "-" << b.size()<< ": "
// // 	   << (*a.find( (*it) ))->id() << " " <<(*it)->id() << endl;
//       tmp.insert( (*it) );
//     }
//   }
//   return tmp;
// }
// SetpTetrahedrons operator + ( const SetpTetrahedrons & a,  const SetpTetrahedrons & b){
//   SetpTetrahedrons tmp( a );
//   for ( SetpTetrahedrons::const_iterator it = b.begin(); it != b.end(); it++ ) tmp.insert( (*it) );
//   return tmp;
// }
// SetpTetrahedrons operator - ( const SetpTetrahedrons & a,  const SetpTetrahedrons & b){
//   SetpTetrahedrons tmp( a );
//   for ( SetpTetrahedrons::const_iterator it = b.begin(); it != b.end(); it++ ) tmp.erase( (*it) );
//   return tmp;
// }

// SetpTetrahedrons operator && ( const SetpTetrahedrons & a,  const SetpTetrahedrons & b){
//   SetpTetrahedrons tmp;
//   for ( SetpTetrahedrons::const_iterator it = b.begin(); it != b.end(); it++ ){
//     if ( a.find( (*it) ) != a.end() ){
// //       cout << a.size() << "-" << b.size()<< ": "
// // 	   << (*a.find( (*it) ))->id() << " " <<(*it)->id() << endl;
//       tmp.insert( (*it) );
//     }
//   }
//   return tmp;
// }
// SetpCells operator + ( const SetpCells & a,  const SetpCells & b){
//   SetpCells tmp( a );
//   for ( SetpCells::const_iterator it = b.begin(); it != b.end(); it++ ) tmp.insert( (*it) );
//   return tmp;
// }
// SetpCells operator - ( const SetpCells & a,  const SetpCells & b){
//   SetpCells tmp( a );
//   for ( SetpCells::const_iterator it = b.begin(); it != b.end(); it++ ) tmp.erase( (*it) );
//   return tmp;
// }
// SetpCells operator && ( const SetpCells & a,  const SetpCells & b){
//   SetpCells tmp;
//   for ( SetpCells::const_iterator it = b.begin(); it != b.end(); it++ ){
//     if ( a.find( (*it) ) != a.end() ){
//       //       cout << a.size() << "-" << b.size()<< ": "
//       // 	   << (*a.find( (*it) ))->id() << " " <<(*it)->id() << endl;
//       tmp.insert( (*it) );
//     }
//   }
//   return tmp;
// }
// SetpTriangleFaces operator || ( const SetpTriangleFaces & a,  const SetpTriangleFaces & b){
//   TO_IMPL
//   TO_CHECK
//   SetpTriangleFaces tmp( a );
//   for ( SetpTriangleFaces::const_iterator it = b.begin(); it != b.end(); it++ ){
//     if ( (*a.find( (*it) )) == NULL ) tmp.insert( (*it ) );
//   }
//   return tmp;
// }

#endif // SETALGORITHM__H

/*
$Log: setalgorithm.h,v $
Revision 1.4  2010/10/26 12:20:28  carsten
win32 compatibility commit

Revision 1.3  2006/01/21 14:33:38  carsten
*** empty log message ***

Revision 1.2  2005/10/24 09:52:08  carsten
*** empty log message ***

Revision 1.1  2005/08/12 16:43:20  carsten
*** empty log message ***

*/
