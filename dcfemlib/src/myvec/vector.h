// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#ifndef MYVEC_VEKTOR__H
#define MYVEC_VEKTOR__H MYVEC_VEKTOR__H

#include "../numfunct.h"
#include "../dcfemlib.h"
using namespace DCFEMLib;

//#include "../log.h"
//#include "../global.h"

#include <iostream>
#include <set>
#include <fstream>
#include <string>
#include <cmath>
#include <algorithm>
#include <cassert>
#include <vector>
#include <valarray>
#include <cstdlib>
#include <cstring>
using namespace std;

namespace MyVec{

template< class T > class Vec;
  //template< class T > class Matrix;
template< class T > class DirectMatrix;
  //template< class T > class BasisMatrix;
}

typedef MyVec::Vec< real_ > RVector;
typedef MyVec::Vec< double > DVector;
typedef MyVec::Vec< float > FVector;

namespace MyVec{

#define BINARY = true

template <class T> class Vec{
public:
  //** Methods
  Vec(){   //** StandartKonstructor
    size_ = 0;
    array_ = new T[ size_] ; assert( array_ );
  }
  ~Vec(){                                  //** Destruktor
    delete [] array_;
  }
  Vec( const int n ) : size_( n ){              //** Konstruktor
    array_ = new T[ size_ ]; assert( array_ );
    this->clear();
  }
  Vec( const int n, T x ) : size_( n ){              //** Konstruktor
    array_ = new T[ size_ ]; assert( array_ );
    for ( int i = 0; i < size_; i ++ ) array_[ i ] = x;
  }

  Vec( const string & filename ) {
    size_ = 1;
    array_ = new T[ size_ ] ; assert( array_ );
    load( filename );
              //** Konstruktor
  }

  Vec( const Vec< T > & a ) : size_( a.size() ){  //** Kopierkonstruktor (f�r Initialisierung)
    array_ = new T[ size_ ]; assert( array_ );
    memcpy( array_, a.array_, size_ * sizeof( T ) );
  }

  Vec< T > & operator = ( const Vec<T> & a ){
    if ( this != &a ){
      delete [] array_;
      size_ = a.size();
      array_ = new T[ size_ ]; assert( array_ );
      memcpy( array_, a.array_, size_ * sizeof( T ) );
    }
    return(*this);
  }

  void clear(){ memset( array_, '\0', size_ * sizeof( T ) ); }

  //** Operatoren

  template< class Matrix > Vec< T > & operator += ( const Matrix & A ){
    assert( (int)size_ >= A.size() );

    for ( int i = 0, imax = A.size(); i < imax; i++ ){
      array_[ A.i( i ) ] += A.at( i, 0 );
    }
    return *this;
  }

  Vec<T> & operator+=(const Vec<T> & a){
    assert(size_ == a.size_);
    for(int i=0; i<size_; i++) array_[i]+=a.array_[i];
    return(*this);
  }
  Vec< T > & operator-=( const Vec< T > & a ){
    assert(size_ == a.size_);
    for( int i = 0; i < size_; i++ ) array_[ i ] -= a.array_[ i ];
    return *this;
  }
  //*!Element by element product. This vector and Vec a must have the same size, unless one of them is sclar.*/
  Vec< T > & operator *= ( const Vec< T > & a ){
    assert( size_ == a.size_ );
    for( int i = 0; i < size_; i++ ) array_[ i ] *= a.array_[ i ];
    return *this;
  }
  Vec< T > & operator/=( const Vec< T > & a ){
    assert( size_ == a.size_ );
    for( int i = 0; i < size_; i++ ) array_[ i ] /= a.array_[ i ];
    return *this;
  }

  Vec< T > & operator *= (const T x){
    //    cout << "Vec::*=OP\t";
    for( int i = 0; i < size_; i++) array_[ i ] *= x;
    return *this;
  }

  Vec<T> & operator/=(const T x){
    //    cout << "Vec::*=OP\t";
    for(int i=0; i<size_; i++) array_[i] /= x;
    return *this;
  }

  Vec<T> & operator+=(const T x){
    //    cout << "Vec::*=OP\t";
    for(int i=0; i<size_; i++) array_[i] += x;
    return *this;
  }

  Vec<T> & operator-=(const T x){
    //    cout << "Vec::*=OP\t";
    for(int i=0; i<size_; i++) array_[i] -= x;
    return *this;
  }

  const T & operator[]( int i ) const { return array_[ i ]; }

  T &operator[]( int i ){ return array_[ i ]; }

  template <class Vector> void copyTo( Vector & v ){
    assert( (int)size_ == v.size() );
    for ( int i = 0; i < size_; i ++ ) v[ i ] = array_[ i ];
  }

  Vec<T> absAllValues(){
    Vec<T> tmp( *this );
    for (int i = 0, imax = size(); i < imax; i++) tmp[i] = fabs( tmp[i] );
    return tmp;
  }

  /*! Workaround until BLAS standartisation. Returns a \ref RVector which is a pointwise multiplication of this and \ref RVector a.*/
  Vec< T > mult( const Vec< T > & a ){
    Vec< T > tmp( a );
    for ( int i = 0, imax = a.size(); i < imax; i ++ ){
      tmp[ i ] *= (*this)[ i ];
    }
    return tmp;
  }


  void setVal(int i, T x){
    assert(i>0 && i<=size_); array_[i-1] = x;
  }
  void addVal(int i, T x){ assert(i>0 && i<=size_); array_[i-1] += x; }
  void divVal(int i, T x){ assert(i>0 && i<=size_); array_[i-1] /= x; }
  void mulVal(int i, T x){ assert(i>0 && i<=size_); array_[i-1] *= x; }
  void subVal(int i, T x){ assert(i>0 && i<=size_); array_[i-1] -= x; }

  T & at(int i) const { assert(i>0 && i<=size_); return(array_[i-1]);  }

  int size() const { return size_; }

  void resize( int n ){
    if ( n != size_ ){
      Vec< T > tmp( *this );
      delete[] array_; array_ = new T[ n ]; assert( array_ );

      for ( int i = 0; i < n; i ++ ){
	if ( i < size_ ){
	  array_[ i ] = tmp[ i ];
	} else {
	  array_[ i ] = 0.0;
	}
      }
// 	Vec<T> tmp(n);
// 	memcpy(tmp.array_, array_, n * sizeof(T));
// 	delete[] array_; array_ = new T[n];
// 	memcpy( array_, tmp.array_, n * sizeof( T ) );
      size_ = n;
    }
  }

  void show( int n ){
    int count = 0;
    for (int i = 0; i < size_; i++){
      cout << array_[ i ] << "\t";
      count++;
      if ( count == n ){ cout << endl; count = 0; }
    }
    cout << endl;
  }

  void show(){ show( 0 ); }

  void showSparse(){
    for ( int i = 0; i < size_; i++ ){
      if ( fabs( array_[ i ] ) > 1E-40 ) cout << i << "\t" << array_[i] << endl;
    }
  }
  int save( const string & str, IOFormat format = Binary ) const { return save( str, 0, size_, format ); }

  int save( const string & str, int min, int max, IOFormat format = Binary) const {
    int count = max - min;
    switch ( format ){
    case Ascii:{
      fstream file;
      if ( !openOutFile( str, & file ) ) return 0;
      file.setf( ios::scientific, ios::floatfield );
      file.precision( 13 );
      for ( int i = min; i < max; i++ ) file << at( i + 1) << endl;
      file.close(); break;}
    case Binary:{
      FILE *file;
      file = fopen( str.c_str(), "w+b" );
      if ( !file ) {
	cerr << str << ": " << strerror( errno ) << " " << errno << endl;
	return 0;
      }

      //      if ( !openOutFile( str, & file, ios::binary ) ) return 0;
      //      file.write( (char*)&count, sizeof( int ) );
      fwrite( (char*)&count, sizeof( int ), 1, file );
      for ( int i = min; i < max; i++ ) fwrite( (char*)&array_[ i ], sizeof( T ), 1, file );
      fclose( file ); break;}
    default : cerr << WHERE_AM_I << " Nothing to do for this outputformat: " << format << endl;
      return 0;
    }
    return 1;
  }

  //  int load( const string & str, IOFormat format = BINARY ){ return this->load( str, format );  }
  //  int load( const string & str ){ return load( str, BINARY, true ); }
  //  int load( const string & str, bool abort ){ return load( str, BINARY, abort ); }
  //  int load( const string & str ){ return load( str, BINARY, false) ;}

  int load( const string & str, bool abort ) { return load( str, Binary, abort ); }
  int load( const string & str, IOFormat format = Binary ) { return load( str, format, false ); }
  int load( const string & str, IOFormat format, bool abort ){ return load( str, format, abort, true );  }
  int load( const string & str, IOFormat format, bool abort, bool verbose ){
    fstream file;
    std::vector< T > dummy;

    switch ( format ){
    case Ascii:
      if ( !openInFile(str, & file, verbose ) ) {
	if ( abort ){
	  cerr << str << ": " << strerror( errno ) << " " << errno << endl;
	  exit( 1 );
	}
	return 0;
      }
      T val;
      while( file >> val ) dummy.push_back( val );
      delete[] array_;
      size_ = dummy.size();
      array_ = new T[ size_ ];
      for ( int i = 0, imax = size_; i < imax; i++ ) array_[ i ] = dummy[ i ];
      file.close(); break;
    case Binary:
      FILE *file;
      file = fopen( str.c_str(), "r+b" );
      if ( !file ) {
	if ( abort ){
	  cerr << str << ": " << strerror( errno ) << " " << errno << endl;
	  exit( 1 );
	}
	return 0;
      }
      int count; fread( &count, sizeof( int ), 1, file );
      delete[] array_;
      array_ = new T[ count ];
      size_ = count;
      fread( array_, sizeof( T ), count, file );
      fclose( file );

//       if ( !openInFile( str, & file ) ) return 0;
//       int count;
//       file.read( (char*)&count, sizeof( int ) );
//       delete[] array_;
//       array_ = new T[ count ];
//       size_ = count;
//       for ( int i = 0; i < size_; i ++ ) file.read( (char*)&array_[ i ], sizeof( T ) );
//       file.close(); break;
      break;
    default : cerr << WHERE_AM_I << " Nothing to do for this intputformat: " << format << endl;
      return 0;
    }
    return 1;
  }

  //** Dates
  T *array_; //** public, da die Operatoren (+, *, usw.) nicht auf private zugreifen k�nnen

private:
  int size_;
};

template< class vector > int operator == ( const vector & a , const vector & b ){
  if ( a.size() != b.size() ) return 0;
  for ( int i = 0; i < a.size(); i ++ ) {
    if ( a[ i ] != b[ i ] ) return 0;
  }
  return 1;
}
template< class vector > int operator != ( const vector & a , const vector & b ){ return !( a == b ); }

template<class T> Vec<T> operator + ( const T x, const Vec<T> & a ){
  Vec<T> tmp(a);
  return tmp += x;
}
template<class T> Vec<T> operator - ( const T x, const Vec<T> & a ){
  Vec<T> tmp( a );
  tmp -= x;
  return tmp *= -1;
}
template<class T> Vec<T> operator * ( const T x, const Vec<T> & a ){
  Vec<T> tmp(a);
  return tmp *= x;
}
template<class T> Vec<T> operator / ( const T x, const Vec<T> & a ){
  Vec<T> tmp( a.size() );
  for ( int i = 0, imax = a.size(); i < imax; i++ ){
    tmp[ i ] = x / a[ i ];
  }
  return tmp ;
}
template<class T> Vec<T> operator + (const Vec<T> & a, const T x){
  Vec<T> tmp(a);
  return tmp += x;
}
template<class T> Vec<T> operator - (const Vec<T> & a, const T x){
  Vec<T> tmp(a);
  return tmp -= x;
}
template<class T> Vec<T> operator * (const Vec<T> & a, const T x){
  Vec<T> tmp(a);
  return tmp *= x;
}
template<class T> Vec<T> operator / (const Vec<T> & a, const T x){
  Vec<T> tmp(a);
  return tmp /= x;
}

template< class T > Vec< T > operator + ( const Vec< T > & a, const Vec< T > & b ){
  assert(a.size() == b.size());
  Vec< T > t( a );
  return t += b;
}
template< class T > Vec< T > operator - ( const Vec< T > & a, const Vec< T > & b ){
  assert(a.size() == b.size());
  Vec< T > t( a );
  return t -= b;
}
template < class T > Vec< T > operator * ( const Vec< T > & a, const Vec< T > & b ){
  assert( a.size() == b.size() );
  //  cout << WHERE_AM_I << endl;
  Vec< T > t( a );
  return t *= b;
}
template< class T > Vec< T > operator / ( const Vec< T > & a, const Vec< T > & b ){
  assert(a.size() == b.size());
  Vec< T > t( a );
  return t /= b;
}

  //#include "vector.t"
template < class T > ostream & operator << ( ostream & str, const MyVec::Vec< T > & a ){
  for ( int i = 0, imax = a.size(); i < imax; i ++ ){
    str << a[i] << "\t";
  }
  return str;
}

// template < class Matrix > Matrix operator*( const Matrix & A, const double v ){
//   Matrix tmp( A );
//   return tmp *= v;
// }
// template < class Matrix > Matrix operator*(  const double v, const Matrix & A ){
//   Matrix tmp( A );
//   return tmp *= v;
// }

template < class Matrix, class Vec > Vec operator * ( const Matrix & A, const Vec & v ){
  Vec tmp( A.size() );

  register double tmp0;

  for( int i = 0, imax = A.size(); i < imax; i++ ){
    tmp0 = 0.0;
    for( int j = 0, jmax = v.size(); j < jmax; j++ ){
      tmp0 +=  A[ i ][ j ] * v[ j ];
    }
    tmp[ i ] = tmp0;
  }

//   for( int i = 0, imax = A.size(); i < imax; i++ ){
//     for( Matrix::const_iterator it = A[ i ].begin(); it != A[ i ].end(); it ++ ){
//       //      tmp[ i ] +=  (*it) * v[ j ];
//     }
//   }

//   for( int i = 0, imax = tmp.size(); i < imax; i++ ){
//     tmp[ i ] = dot( A[ i ], v );
//   }
  return tmp;
}

template <class Matrix, class Vec> Vec mult( const Matrix & A, const Vec & v ){
  //  cout << A.size() << "\t" << A[0].size() << "\t" << v.size() << endl;
  Vec tmp( A.size() );
  register double tmp0;

  for( int i = 0, imax = A.size(); i < imax; i++ ){
    tmp0 = 0.0;
    for( int j = 0, jmax = v.size(); j < jmax; j++ ){
      tmp0 +=  A[ i ][ j ] * v[ j ];
    }
    tmp[ i ] = tmp0;
  }

  return tmp;
}

template <class Matrix, class Vec> Vec transMult( const Matrix & A, const Vec & v ){
  //  cout << A.size() << "\t" << A[0].size() << "\t" << v.size() << endl;
  Vec tmp( A[ 0 ].size() );

  for( int i = 0, imax = v.size(); i < imax; i++ ){
    //    cout << i << " " << A[ i ].size() << endl;
    for( int j = 0, jmax = A[ 0 ].size(); j < jmax; j++ ){
      tmp[ j ] +=  A[ i ][ j ] * v[ i ];
    }
  }

  return tmp;
}

//** conflicts with some stl-classes
// template < class Vec > bool operator == ( const Vec & a, const Vec & b ){
//   if ( a.size() != b.size() ) return false;
//   for ( int i = 0; i < a.size(); i ++ ){
//     if ( a[ i ] != b[ i ] ) return false;
//   }
//   return true;
// }
// template < class Vec > bool operator != ( const Vec & a, const Vec & b ){
//   return ( !(a == b) );
// }

template < class Vec > Vec exp( const Vec & a ){
  Vec result( a.size() );
  for ( int i = 0, imax = a.size(); i < imax; i ++ ){
    result[ i ] = std::exp( a[ i ] );
  }
  return result;
}
template < class Vec > Vec asin( const Vec & a ){
  Vec result( a.size() );
  for ( int i = 0, imax = a.size(); i < imax; i ++ ){
    result[ i ] = std::asin( a[ i ] );
  }
  return result;
}
template < class Vec > Vec cos( const Vec & a ){
  Vec result( a.size() );
  for ( int i = 0, imax = a.size(); i < imax; i ++ ){
    result[ i ] = std::cos( a[ i ] );
  }
  return result;
}
template < class Vec > Vec log( const Vec & a ){
  Vec result( a.size() );
  for ( int i = 0, imax = a.size(); i < imax; i ++ ){
    result[ i ] = std::log( a[ i ] );
  }
  return result;
}
template < class Vec > Vec log10( const Vec & a ){
  Vec result( a.size() );
  for ( int i = 0, imax = a.size(); i < imax; i ++ ){
    result[ i ] = std::log10( a[ i ] );
  }
  return result;
}
template < class Vec > Vec absolute( const Vec & a ){
  Vec result( a.size() );
  for ( int i = 0, imax = a.size(); i < imax; i ++ ){
    result[ i ] = std::fabs( a[ i ] );
  }
  return result;
}

template < class Vec > Vec sqrt( const Vec & a ){
  Vec result( (const int)a.size() );
  for ( int i = 0, imax = a.size(); i < imax; i ++ ){
    result[ i ] = std::sqrt( std::fabs( a[ i ] ) );
  }
  return result;
}

template < class Vec > double sum( const Vec & a ) {
  double result = 0.0;
  for ( int i = 0, imax = a.size(); i < imax; i ++ ){
    result += a[ i ];
  }
  return result;
}
// template < class Vec > Vec cross( const Vec & a, const Vec & b ){
//   return a * b;
// }
template < class Vec > Vec square( const Vec & a ) {
  return a * a;
}
template < class Vec > void save( const Vec & a, const string & filename ){
  ofstream file; file.open( filename.c_str() );
  for ( int i = 0, imax = a.size(); i < imax; i ++ ){
    file << a[ i ] << endl;
  }
  file.close();
}
template < class Vec > Vec load( const string & filename ){
  Vec a;
  double tmp = 0.0;
  ifstream file; file.open( filename.c_str() );
  while( file >> tmp ) a.push_back( tmp );
  file.close();
  return a;
}
template < class Vec > double scalar( const Vec & a, const Vec & b ){
  return sum( a * b );
}
template < class Vec > double dot( const Vec & a, const Vec & b ){
  return scalar( a, b );
}

template < class Vec > Vec sort( const Vec & a ) {
  //** Q&D
  vector < double > tmp;
  for ( int i = 0; i < (int)a.size(); i ++ ) tmp.push_back( a[ i ] );
  sort( tmp.begin(), tmp.end() );
  Vec ret( a.size() );
  for ( int i = 0; i < (int)a.size(); i ++ ) ret[ i ] = tmp[ i ];
  return ret;
}

template < class Vec > Vec uniqI( const Vec & a ) {
  // ** still hack for int
  std::set< int > tmpSet;
  for ( uint i = 0; i < a.size(); i ++ ) tmpSet.insert( a[ i ] );
  Vec tmp;
  for ( std::set<int>::iterator it = tmpSet.begin(); it != tmpSet.end(); it ++ ) tmp.push_back( *it );
  return tmp;
}

template < class Vec > Vec uniqD( const Vec & a ) {
  // ** still hack for int
  std::set< double > tmpSet;
  for ( uint i = 0; i < a.size(); i ++ ) tmpSet.insert( a[ i ] );
  Vec tmp;
  for ( std::set<double>::iterator it = tmpSet.begin(); it != tmpSet.end(); it ++ ) tmp.push_back( *it );
  return tmp;
}

template < class Vec > double median( const Vec & a ) {
  int dim = a.size();
  Vec tmp = sort( a );
  if ( fabs( dim / 2.0 - rint( dim / 2.0 ) ) < 1e-12 ){ // even
    return ( tmp[ dim / 2 ] + tmp[ dim / 2 - 1] ) / 2.0;
  } else { // odd
    return tmp[ ( dim - 1 )/ 2 ];
  }
}
template < class Vec > double mean( const Vec & a ) {
  return sum( a ) / ( a.size() );
}
template < class Vec > double arithmeticMean( const Vec & a ) {
  return mean( a );
}
template < class Vec > double geometricMean( const Vec & a ) {
   int dim = a.size();
   double result = 0.0;
   for ( int i = 0; i < dim; i ++ ) result += std::log( a[ i ] );
   result /= (double)dim;
   return std::exp( result );
}
template < class Vec > double harmonicMean( const Vec & a ) {
  int dim = a.size();
  double result = 1.0 / a[ 0 ];

  for ( int i = 1; i < dim; i ++ ) result += 1.0 / a[ i ];
  result /= dim;
  return 1.0 / result;
}
// template < template class Vec< class T > > T min( const Vec< T > & a ) {
//   T tmp = a[ 0 ];
//   for ( int i = 1; i < a.size(); i ++ ){
//     if ( a[ i ] < tmp ) tmp = a[ i ];
//   }
//   return tmp;
// }

template < class Vec > double min( const Vec & a ) {
  double tmp = a[ 0 ];
  for ( int i = 1; i < a.size(); i ++ ){
    if ( a[ i ] < tmp ) tmp = a[ i ];
  }
  return tmp;
}
template < class Vec > double max( const Vec & a ) {
  double tmp = a[ 0 ];
  for ( int i = 1; i < a.size(); i ++ ){
    if ( a[ i ] > tmp ) tmp = a[ i ];
  }
  return tmp;
}
template < class T > T min( const std::vector< T > & a ) {
  T tmp = a[ 0 ];
  for ( uint i = 1; i < a.size(); i ++ ){
    if ( a[ i ] < tmp ) tmp = a[ i ];
  }
  return tmp;
}
template < class T > T max( const std::vector< T > & a ) {
  T tmp = a[ 0 ];
  for ( uint i = 1; i < a.size(); i ++ ){
    if ( a[ i ] > tmp ) tmp = a[ i ];
  }
  return tmp;
}
template < class Vec > double epsilon( const Vec & a, bool withoutZero = false ){
  if ( withoutZero ){
    double epsilon = 1e99;
    double tmp = 0.;
    for ( int i = 0; i < a.size(); i++ ){
      if ( a[ i ] != 0.0 ){
 	if ( ( tmp = fabs( a[ i ] ) ) < epsilon ) epsilon = tmp;
      }
    }
    return epsilon;
  } else return min( absolute( a ) );
}

template < class Vec > double normH0( const Vec & a ) {
  TO_IMPL;
  return 0.0;
}
template < class Vec > double normH1( const Vec & a ) {
  TO_IMPL;
  return 0.0;
}
template < class Vec > double semiNormH1( const Vec & a ) {
  TO_IMPL;
  return 0.0;
}
template < class Vec > Vec pow( const Vec & a, double p ) {
  Vec tmp( a.size() );
  for ( int i = 0; i < a.size(); i ++ ) tmp[ i ] = std::pow( a[ i ], (double)p );
  return tmp;
}
template < class Vec > double normlp( const Vec & a, int p ) {
  return std::pow( sum( pow( absolute( a ), p) ), 1.0/(double)p );
}
template < class Vec > double norml1( const Vec & a ) {
  //http://mathworld.wolfram.com/L1-Norm.html
  //  sum( absolute( a ) );
  return normlp( a, 1 );
}
template < class Vec > double norml2( const Vec & a ) {
  // vector norm \ell^2 nicht L^2
  return normlp( a, 2 );
}
template < class Vec > double normlInfinity( const Vec & a ) {
  return max( absolute( a ) );
}
template < class Vec > double euclideanNorm( const Vec & a ) {
  return norml2( a );
}
template < class Vec > double norm( const Vec & a ) {
  //** sqrt( a_0^2 + a_i^2 + a_n^2) ; 0 < i < a.size()
  return norml2( a );
}
template < class Vec > double rms( const Vec & a ) {
//return std::sqrt( mean( square( Vec( a - mean( a ) ) ) ) );
    return std::sqrt( mean( square( Vec( a) ) ) );
}

template < class Vec > double rms( const Vec & a, const Vec & b ) {
  return std::sqrt( mean( square( a - b ) ) );
}

template < class Vec > double rrms( const Vec & a,  const Vec & b ) {
  return std::sqrt( mean( square( (a - b ) / a ) ) );
}

template < class Vec > bool isZero( const Vec & a ) {
  for ( int i = 0, imax = a.size(); i < imax; i ++ ){
    if ( std::fabs( a[ i ] ) > 1e-18 ){
      return false;
    }
  }
  return true;
}

template < class Vector, class Real > void fill( Vector & vec, Real val ){
  for ( int i = 0, imax = vec.size(); i < imax; i ++ ){
    vec[ i ] = val;
  }
}
template < class Vector > Vector append( const Vector & v1, const Vector & v2){
  Vector tmp( v1.size() + v2.size() );
  for ( int i = 0, imax = v1.size(); i < imax; i ++ ){
    tmp[ i ] = v1[ i ];
  }
  for ( int i = 0, imax = v2.size(); i < imax; i ++ ){
    tmp[ v1.size() + i ] = v2[ i ];
  }
  return tmp;
}
template < class Vector, class Real > void rand( Vector & vec, Real min = 0.0, Real max = 1.0){
#ifdef MINGW
TO_IMPL // dont work with minGW
//   for ( int i = 0, imax = vec.size(); i < imax; i ++ ){
//     vec[ i ] = rand() * ((max -min)/ RAND_MAX) + min;
//   }
#else
  for ( int i = 0, imax = vec.size(); i < imax; i ++ ){
    vec[ i ] = random() * ((max -min)/ RAND_MAX) + min;
  }
#endif
}
template < class Vector > void randomize( Vector & vec, double min = 0.0, double max = 1.0 ){
  return rand< Vector, double >( vec, min, max );
}
template < class Vector > void randn( Vector & vec ){
#ifdef MINGW_BLA
TO_IMPL // dont work with minGW
#else
//   randomize( vec );
//   vec = sqrt( log( 1.0 - vec ) * -2.0 ) * cos( vec * 2.0 * PI_);
  //** keine Ahnung was passiert aber es funktioniert. vgl. tests/vector/baseio.cpp
  for ( int i = 0, imax = vec.size(); i < imax; i ++ ){
    double sum = 0;
    for ( int j = 0; j < 16; j++ ) sum += std::rand() & 0xfff;
    vec[ i ] = (sum - 0x8000) * (1.0 / 4729.7);
  }
#endif
}

template < class Vec > bool linearlyDependend( const Vec & a, const Vec & b ){
  double tolerance = 1e-12;
  Vec tmp( a / b );
  for ( int i = 0, imax = tmp.size() -1; i < imax; i ++ ){
    if ( fabs( tmp[ i ] - tmp[ i + 1 ] ) > tolerance ) return false;
  }
  return true;
}

template < class Matrix > Matrix transpose( const Matrix & A ) {
  Matrix tmp( A.dim2(), A.dim1() );

  for( int i = 0, imax = tmp.dim1(); i < imax; i++ ){
    for( int j = 0, jmax = tmp.dim2(); j < jmax; j++ ){
      tmp[ i ][ j ] = A[ j ][ i ];
    }
  }
  return tmp;
}

} // namespace MyVec
#endif  //** MYVEC_VECTOR__H

/*
$Log: vector.h,v $
Revision 1.36  2011/08/30 14:09:51  carsten
fix memory problem within mesh-copy

Revision 1.35  2008/05/27 11:32:50  carsten
*** empty log message ***

Revision 1.34  2008/05/19 14:52:28  carsten
Add cholmodwrapper, interpolate V.2 in createSurface

Revision 1.33  2008/04/17 12:24:36  thomas
no message

Revision 1.32  2008/03/11 13:59:28  thomas
no message

Revision 1.31  2007/10/04 14:35:02  carsten
*** empty log message ***

Revision 1.30  2007/10/04 07:21:59  thomas
BERT + polytools codeblocks (dllexport)

Revision 1.29  2007/04/19 18:42:54  carsten
*** empty log message ***

Revision 1.28  2007/04/10 12:01:36  carsten
*** empty log message ***

Revision 1.27  2007/03/15 18:04:14  carsten
*** empty log message ***

Revision 1.26  2007/01/08 21:41:53  carsten
*** empty log message ***

Revision 1.25  2006/11/03 13:25:40  carsten
*** empty log message ***

Revision 1.24  2006/07/20 16:05:47  carsten
*** empty log message ***

Revision 1.23  2006/05/31 22:13:50  carsten
*** empty log message ***

Revision 1.22  2006/05/29 19:42:04  carsten
*** empty log message ***

Revision 1.21  2006/05/08 16:25:59  carsten
*** empty log message ***

Revision 1.20  2006/03/13 18:04:25  carsten
*** empty log message ***

Revision 1.19  2006/02/10 23:16:20  carsten
*** empty log message ***

Revision 1.18  2006/01/31 18:27:58  carsten
*** empty log message ***

Revision 1.17  2005/10/17 15:10:17  carsten
*** empty log message ***

Revision 1.16  2005/10/17 11:56:58  carsten
*** empty log message ***

Revision 1.15  2005/10/12 14:41:56  carsten
*** empty log message ***

Revision 1.14  2005/09/29 17:58:26  carsten
*** empty log message ***

Revision 1.13  2005/09/28 17:43:20  carsten
*** empty log message ***

Revision 1.12  2005/08/09 18:13:44  carsten
*** empty log message ***

Revision 1.11  2005/07/13 17:05:21  carsten
*** empty log message ***

Revision 1.10  2005/06/02 15:44:10  carsten
*** empty log message ***

Revision 1.9  2005/03/08 16:04:45  carsten
*** empty log message ***

Revision 1.8  2005/02/14 19:37:32  carsten
*** empty log message ***

Revision 1.7  2005/02/11 17:12:24  carsten
Friday commit

Revision 1.6  2005/01/13 20:10:42  carsten
*** empty log message ***

Revision 1.5  2005/01/11 19:12:26  carsten
*** empty log message ***

Revision 1.4  2005/01/06 20:28:30  carsten
*** empty log message ***

Revision 1.3  2005/01/05 13:29:16  carsten
*** empty log message ***

Revision 1.2  2005/01/04 19:44:05  carsten
*** empty log message ***

Revision 1.1  2005/01/04 19:23:18  carsten
*** empty log message ***

*/
