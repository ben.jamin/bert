// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#include "dcfemlib.h"

#include "datamap.h"
#include "elements.h"
#include "mesh2d.h"
using namespace MyMesh;

#include "stlmatrix.h"
using namespace MyVec;

#include "solver.h"

#include "electrodeconfig.h"
#include <sstream>
using namespace std;
// #include "vektor/vektor.h"
// #include "numfunct.h"

double standardisedPotential( const RealPos & sourcePos, const RealPos & destPos,
			      SpaceConfigEnum config, double mirrorPlaneZ ){

  double radius = 0.0, radiusMirror = 0.0, result = 0.0; 
  radius = sourcePos.distance( destPos );  

  if ( radius < TOLERANCE * TOLERANCE ){ 
    result = 0.0; 
  } else { 
    switch( config ){
    case HALFSPACE: 
      result = 1.0 / ( 2.0 * PI_ * radius );
      break;
    case FULLSPACE: 
      result = 1.0 / ( 4.0 * PI_ * radius );
      break;
    case MIRRORSOURCE: {
      RealPos sourcePosMirror( sourcePos );
      sourcePosMirror.setZ( 2.0 * mirrorPlaneZ - sourcePos.z() );
      radiusMirror = sourcePosMirror.distance( destPos );
      //std::cout << sourcePos << " " << sourcePosMirror << std::endl;
      result = 1.0 / ( 4.0 * PI_ ) * ( 1.0 / radius + 1.0 / radiusMirror );
      break;
    }
    case PLANE: // like HALFSPACE without z-koordinate
//       std::cout << "PLANE" << std::endl;
//       exit(1);
      //      funktioniert nur bei 3D
      radius = sqrt( pow( sourcePos.x() - destPos.x(), 2.0 ) + pow( sourcePos.y() - destPos.y(), 2.0 ) );
      result = 1.0 / ( 2.0 * PI_ * radius );
      break;
    case PLANE2D: // like HALFSPACE without y and z-koordinate
      //      funktioniert nur bei 2D
      radius = sqrt( pow( sourcePos.x() - destPos.x(), 2.0 ) );
      //    cout << radius << endl;
      result = 1.0 / ( 2.0 * PI_ * radius );
      break;
    }
  }

  return result;
}

double standardisedPotential( const RealPos & a, const RealPos & b,
			      const RealPos & m, const RealPos & n,
			      SpaceConfigEnum config, double mirrorPlaneZ ){

  double am = 0.0, an = 0.0, bm = 0.0, bn = 0.0;

  if ( a.valid() && m.valid() ) am = standardisedPotential( a, m, config, mirrorPlaneZ );
  if ( a.valid() && n.valid() ) an = standardisedPotential( a, n, config, mirrorPlaneZ );
  if ( b.valid() && m.valid() ) bm = standardisedPotential( b, m, config, mirrorPlaneZ );
  if ( b.valid() && n.valid() ) bn = standardisedPotential( b, n, config, mirrorPlaneZ );

  double result = ( am - an ) - ( bm - bn );
  
  if ( fabs(result) < 1e-16 ){
    cerr << WHERE_AM_I << " found u = 0.0, that leads to infinity geometric factor: " << (int)config<<  endl;
    cerr << a << b << m << n << endl;
    cerr << "am: " << am << " an: " << an << " bm: " << bm << " bn: " << bn << endl;
  }
  return result;
}

RVector standardisedPotential( const RealPos & sourcePos, const vector< Node *> & nodeVec,
			       SpaceConfigEnum config, double mirrorPlaneZ ){
  RVector solution( nodeVec.size() );

  for ( size_t i = 0, imax = nodeVec.size(); i < imax; i++ ){
    solution[ i ] = standardisedPotential( sourcePos, nodeVec[ i ]->pos(), config, mirrorPlaneZ ); 
  }
  return solution;
}

RVector standardisedPotential( const RealPos & sourcePos, const vector< Node > & nodeVec,
			       SpaceConfigEnum config, double mirrorPlaneZ ){
  RVector solution( nodeVec.size() );

  for ( size_t i = 0, imax = nodeVec.size(); i < imax; i++ ){
    solution[ i ] = standardisedPotential( sourcePos, nodeVec[ i ].pos(), config, mirrorPlaneZ ); 
  }
  return solution;
}

RVector standardisedPotential( const RealPos & sourcePos, const BaseMesh & mesh,
			       SpaceConfigEnum config, double mirrorPlaneZ ){
  return standardisedPotential( sourcePos, 
				const_cast<BaseMesh & >( mesh ).pNodesVector(), 
				config, mirrorPlaneZ );
}

double standardisedPotential( const ElectrodeConfig & econfig, const DataMap & datamap,
			      SpaceConfigEnum config, double mirrorPlaneZ ){

  cout << WHERE_AM_I << " do not use until redesign." << endl;
  exit(1);
  RealPos a; //if ( econfig.a() > -1 ) a = *econfig.eA();
  RealPos b; //if ( econfig.b() > -1 ) b = *econfig.eB();
  RealPos m; //if ( econfig.m() > -1 ) m = *econfig.eM();
  RealPos n; //if ( econfig.n() > -1 ) n = *econfig.eN();
  
  return standardisedPotential( a, b, m, n, config, mirrorPlaneZ );
}

double standardisedPotential( const ElectrodeConfigVector & profile, int i,
			       SpaceConfigEnum config, double mirrorPlaneZ ){
  RealPos a; if ( profile[ i ].a() > -1 ) a = profile.posA( i );
  RealPos b; if ( profile[ i ].b() > -1 ) b = profile.posB( i );
  RealPos m; if ( profile[ i ].m() > -1 ) m = profile.posM( i );
  RealPos n; if ( profile[ i ].n() > -1 ) n = profile.posN( i );
  
  return standardisedPotential( a, b, m, n, config, mirrorPlaneZ );
}

int DataMap::save( const string & filename ){
  fstream outfile; openOutFile( filename, & outfile );
  int nelectrodes = size(); 
  outfile << nelectrodes << "\t" << dipole_ <<  endl;

  outfile.precision( 14 );
  for ( int i = 0; i < nelectrodes; i ++){
    outfile << vecSourcePos_[ i ].x() << "\t"
	    << vecSourcePos_[ i ].y() << "\t"
	    << vecSourcePos_[ i ].z() << endl;
  }
  for ( int i = 0; i < nelectrodes; i ++){
    outfile << i << "\t" ;
    outfile.setf( ios::scientific, ios::floatfield );
    outfile.precision( 14 );
    for ( int j = 0; j < nelectrodes; j ++){
      outfile << (*this)[ i ][ j ] << "\t";
    }
    outfile << endl;
  }
  outfile.close();
  return 1;
}

int DataMap::load( const string & filename ){
  clearAll();
  fstream file; if ( !openInFile( filename, & file ) ) return 0;
  vector < string > row; row = getNonEmptyRow( file );

  int nElectrodes = toInt( row[ 0 ] );
  
  allocateSpace_( nElectrodes );
  double x = 0.0, y = 0.0, z = 0.0;

  for ( int i = 0; i < nElectrodes; i ++){
    row = getNonEmptyRow( file );
    switch( row.size() ){
    case 1: x = toDouble( row[ 0 ] ); break;
    case 2: x = toDouble( row[ 0 ] ); y = toDouble( row[ 1 ] ); break;
    case 3: x = toDouble( row[ 0 ] ); y = toDouble( row[ 1 ] ); z = toDouble( row[ 2 ] ); break;
    default: cerr << WHERE_AM_I << " row size unknown " << row.size() << endl;
    }
    sourceNodes_.push_back( Node( x, y, z ) );
    vecSourcePos_.push_back( RealPos( x, y, z ) );
  }
 
  for ( int i = 0; i < nElectrodes; i ++){
    row = getNonEmptyRow( file );
    int offset = row.size() - nElectrodes;
    if ( offset < 0 ) {
      std::cerr << WHERE_AM_I << " format wrong" << std::endl;
    }
    for ( int j = 0; j < nElectrodes; j ++){
      (*this)[ i ][ j ] = toDouble( row[ j + offset ] );
    }
  }
  
  file.close();
  return 1;
}

void DataMap::collectData( const vector < RVector > & data ){
  if ( this->size() == data.size() ){
    cerr << WHERE_AM_I << " please allocate Memory using setElectrodePositions(..)" << endl;
  }
  for ( size_t i = 0; i < this->size(); i ++ ){
    for ( size_t j = 0; j < sourceNodes_.size(); j ++){
      (*this)[ i ][ j ] = data[ i ][ sourceNodes_[ j ].id() ];
    }
  }
  
}

void DataMap::importSolution( const string & fileBody ){
  int imax = 0;

  if ( dipole_ && !circle_ ){
    imax = this->size() -1;
  } else {
    imax = this->size();
  }
  RVector pot;
  
  for ( int i = 0; i < imax; i ++ ){
    if ( !pot.load( fileBody + "." + toStr( i ) + ".pot" ) ) {
      cerr << "Gap detected. Electrode Data " << i << " missing " << endl;
      pot.clear();
    }
    this->importSolution(i, pot);
  }
}

// void DataMap::createSoundingFromProfile( const ElectrodeConfigVector & profile ){
//   WHERE_AM_I
// }

void DataMap::importSolution( const vector < RVector > & solution ){
//   if ( this->size() != solution.size() ){
//     cerr << WHERE_AM_I << " this->size() != solution.size()" << this->size() << "!=" << solution.size() << endl;
//     exit(1);
//   }
  for ( size_t i = 0; i < solution.size(); i ++ ){
    importSolution( i, solution[ i ] );
  }
}

void DataMap::importSolution( int position, const RVector & solution ){
  if ( sourceNodes_.size() > 0 ){
    for ( size_t i = 0; i < sourceNodes_.size(); i ++){
      (*this)[ position ][ i ] = solution[ sourceNodes_[ i ].id() ];
    }
  } else {
    for ( size_t i = 0; i < vecSourcePos_.size(); i ++ ){
      (*this)[ position ][ i ] = findNearestSolution( i, solution );
    }
  }
}

double DataMap::findNearestSolution( int posIdx, const RVector & solution ){
  TO_IMPL
//   RealPos queryPos = vecSourcePos_[ posIdx ];

//   //  cout << vecSampleNodes_.size() << endl;

//   vector < Node * > sampleNodes( vecSampleNodes_[ posIdx ] );

//   if ( sampleNodes.size() == 0 ){

//     for ( int i = 0; i < mesh_->boundaryCount(); i ++ ){
//       if ( mesh_->boundary( i ).marker() == -1 ){
// 	switch( mesh_->boundary( i ).touch( queryPos ) ){
// 	case 2:
// 	  sampleNodes.push_back( & dynamic_cast< Edge & >( mesh_->boundary( i ) ).nodeA() );
// 	  break;
// 	case 3:
// 	  sampleNodes.push_back( & dynamic_cast< Edge & >( mesh_->boundary( i ) ).nodeA() );
// 	  sampleNodes.push_back( & dynamic_cast< Edge & >( mesh_->boundary( i ) ).nodeB() );
// 	  break;
// 	case 4:
// 	  sampleNodes.push_back( & dynamic_cast< Edge & >( mesh_->boundary( i ) ).nodeB() );
// 	  break;
// 	}
// 	if ( sampleNodes.size() > 0 ) break;
//       }
//     }

//     if ( sampleNodes.size() == 0 ) {
//       sampleNodes = mesh_->findNearestCell( queryPos ).nodesVector();
//     }

//     vecSampleNodes_[ posIdx ] = sampleNodes;
//   } 

//   //  cout << sampleNodes.size() << endl;
//   if ( sampleNodes.size() == 1 ){

//     return solution[ sampleNodes[ 0 ]->id() ];

//   } else if ( sampleNodes.size() == 2 ){

//   double radius = sampleNodes[ 0 ]->pos().distance( sampleNodes[ 1 ]->pos() );
//   double rmin = sampleNodes[ 0 ]->pos().distance( queryPos );
  
//   double v0 = solution[ sampleNodes[ 0 ]->id() ];  
//   double v1 = solution[ sampleNodes[ 1 ]->id() ];  
    
//   return ( v0 + ( (v1-v0) / radius ) * rmin );
  
//   } else if ( sampleNodes.size() == 3 ){

//     RealPos p1( sampleNodes[ 0 ]->pos() );
//     RealPos p2( sampleNodes[ 1 ]->pos() );
//     RealPos p3( sampleNodes[ 2 ]->pos() );
    
//     STLVector u( 3 );
//     STLMatrix A( 3, 3 );
//     STLVector c( 3 );

//     for ( int i = 0; i < 3; i ++ ) u[ i ] = solution[ sampleNodes[ i ]->id() ];
    
//     A[ 0 ][ 0 ] = 1.0;  A[ 0 ][ 1 ] = p1.x(); A[ 0 ][ 2 ] = p1.y(); 
//     A[ 1 ][ 0 ] = 1.0;  A[ 1 ][ 1 ] = p2.x(); A[ 1 ][ 2 ] = p2.y(); 
//     A[ 2 ][ 0 ] = 1.0;  A[ 2 ][ 1 ] = p3.x(); A[ 2 ][ 2 ] = p3.y(); 
    
//     solveLU( A, c, u );
  
//     return ( c[ 0 ] + c[ 1 ] * queryPos.x() + c[ 2 ] * queryPos.y() );
//   }

//   cerr << WHERE_AM_I << " something goes wrong, samplePoints.size() = " << sampleNodes.size() << endl;
//   exit(0);
  

  return 0.0;
}

int DataMap::findElectrodePosition( const BaseMesh & mesh, bool sort ){
  clearAll();

  vector< int > sources;
  for ( int i = 0; i < mesh.nodeCount(); i ++){
    if ( mesh.node(i).marker() == -99 ) sources.push_back( i );
  }
  
  if ( sort ){
    RealPos startPos( -1e10, -1e10, 0.0);
    map < double, int > posMap;
    for ( size_t i = 0; i < sources.size(); i ++){
      posMap[ startPos.distance( mesh.node( sources[ i ] ).pos() ) ] = sources[ i ];
    }
    sources.clear();
    for ( map < double, int >::iterator it = posMap.begin(); it != posMap.end(); it++ ){
      sources.push_back( it->second );
    }
  }

  for ( int i = 0, imax = sources.size(); i < imax; i ++){
    sourceNodes_.push_back( Node( mesh.node( sources[ i ] ) ) );
  }
  for ( int i = 0, imax = sourceNodes_.size(); i < imax; i ++){
    push_back( RVector( imax ) );
  }
  return 1;
}

RVector DataMap::vectorOfDistancesFrom( int source ){
  RVector VecDistances( size() );
  for ( int i = 0, imax = size(); i < imax; i ++ ){
    VecDistances[ i ] = sourceNodes_[ source ].pos().distance( sourceNodes_[ i ].pos() );
  }
  return VecDistances;
}

RVector DataMap::data( const ElectrodeConfigVector & prof, bool reciprocity ) const {
  //** seachEPos = false, d.h. die Sammlung geht nur �ber die Indexnummern, gibt Probleme wenn die Elektrodenliste in einer falschen Reihenfolge im Netz steht. das wird dringend neu gemacht.

  bool searchEPos = false;
  map < int, int > elecsIdxMatch;
  ElectrodeConfigVector profile( prof );
  if ( profile.electrodeCount() == 0 ) {
    profile.setElectrodePositions( (*this) );
  }
  
  vector < RealPos > profElecs( profile.electrodePositions( true ) );
    
  int nElecs = (size_t)profElecs.size();
  
  if ( searchEPos ){
    //** sammle alle im Profil genutzten Elektroden.;
    set < int > usedElecs;
    for ( int i = 0, imax = profile.size(); i < imax; i ++ ){
      usedElecs.insert( profile[ i ].a() );
      usedElecs.insert( profile[ i ].b() );
      usedElecs.insert( profile[ i ].m() );
      usedElecs.insert( profile[ i ].n() );
    }
    usedElecs.erase( -1 ); //** this is no electrode;
    
    if ( usedElecs.size() > sourceNodes_.size() ){
      cerr << WHERE_AM_I << " there are more electrodes used, than i know" 
	   << usedElecs.size() << " > " << this->size() << endl;
      //    exit(1);
    }
    
    //** sortiere die Elektroden nach index der datamap nach den genutzten;
    bool found = false;
    
    for ( set < int >::iterator it = usedElecs.begin(); it != usedElecs.end(); it ++ ){
      found = false;
      //    cout << *it << " " << profElecs[ *it ] << " " << sourceNodes_[ *it ].pos();
      
      for ( int i = 0, imax = this->size(); i < imax; i ++ ){
	if ( profElecs[ *it ].round( 1e-2 ) == sourceNodes_[ i ].pos().round( 1e-2 ) ){
	  //	cout << i << " " << sourceNodes_[ i ].pos() << endl;
	  found = true;
	  elecsIdxMatch[ *it ] = i;
	  break;
	}
      }
      
      if ( ! found ) {
	cerr << WHERE_AM_I << " requestet e-pos: " << profElecs[ *it ] << " not found in datamap." << endl;
	exit(0);
      }
    }
  }

  int a = 0, b = 0, m = 0, n = 0;
  RVector data( profile.size() );
  for ( int i = 0, imax = profile.size(); i < imax; i ++ ){

    if ( searchEPos ){
      if ( profile[ i ].a() != -1 ) a = elecsIdxMatch[ profile[ i ].a() ]; else a = -1;
      if ( profile[ i ].b() != -1 ) b = elecsIdxMatch[ profile[ i ].b() ]; else b = -1;
      if ( profile[ i ].m() != -1 ) m = elecsIdxMatch[ profile[ i ].m() ]; else m = -1;
      if ( profile[ i ].n() != -1 ) n = elecsIdxMatch[ profile[ i ].n() ]; else n = -1;
    } else {
      a = profile[ i ].a();
      b = profile[ i ].b();
      m = profile[ i ].m();
      n = profile[ i ].n();
    }

    if ( reciprocity ){
      swapVal( a, m );
      swapVal( b, n );
    }
    
    if ( ( a > nElecs-1 ) || ( a < -1 ) ||
	 ( b > nElecs-1 ) || ( b < -1 ) ||
	 ( m > nElecs-1 ) || ( m < -1 ) ||
	 ( n > nElecs-1 ) || ( n < -1 ) ) {
      cerr << WHERE_AM_I << " Warning! Collect matrix to small. Number of electrodes = "  << nElecs  
	   << "; data: " << i << " ; a = " << a << " b = " << b << " m = " << m << " n = " << n << endl;
      exit(1);
    } 

    double uAM = 0.0, uAN = 0.0, uBM = 0.0, uBN = 0.0;
    if ( a != -1 && m != -1) uAM = (*this)[ a ][ m ];
    if ( a != -1 && n != -1) uAN = (*this)[ a ][ n ];
    if ( b != -1 && m != -1) uBM = (*this)[ b ][ m ];
    if ( b != -1 && n != -1) uBN = (*this)[ b ][ n ];
    
//if (b == 5070 ){
//	(*this)[b].save("b5070.vec", Ascii);
//    cout << a << " " << b << " "<< m << " "<< n << " "
// 	 << uAM  << " " << uAN  << " " << uBM  << " " << uBN << endl;
 //   
 //    cout << ( uAM - uAN ) - ( uBM - uBN ) << endl;
//     exit(0);
//}
    data[ i ] = ( uAM - uAN ) - ( uBM - uBN );
  }
  
  return data;
}

int DataMap::saveSelectedData( const string & outfilename, const ElectrodeConfigVector & profIn, bool fromOne,
			       SpaceConfigEnum spaceConfig, double mirrorPlaneZ ){

  ElectrodeConfigVector profile( profIn );
  if ( profile.electrodeCount() == 0 ) profile.setElectrodePositions( (*this) );

  profile.setU( this->data( profile ) );
  string filename = outfilename;

  if ( !circle_ ){
    for ( int i = 0, imax = profile.size(); i < imax; i ++ ){
      if ( profile[ i ].k() == 0 ){ 
	profile[ i ].setK( 1.0 / standardisedPotential( profile, i, spaceConfig, mirrorPlaneZ ) );
      }
    }
    profile.setRhoA( profile.u() * profile.k() ); // i default = 1.0;
    profile.save( filename, "a b m n rhoa", fromOne );
  } else {
    profile.setR( profile.u() / profile.i() );
    profile.save( filename.substr( 0, filename.rfind(".dat")) + ".ohm", "a b m n r", fromOne );
  }
  return 1;
}

int DataMap::saveConfigList( const string & outfilename, const string & infilename, 
			      SpaceConfigEnum spaceConfig, double mirrorPlaneZ ){
  bool fromOne = true;
  ElectrodeConfigVector profil( verbose_ ); profil.load( infilename, fromOne );
  
  return saveSelectedData( outfilename, profil, fromOne, spaceConfig, mirrorPlaneZ );
}


int DataMap::savePolPolData( const string & filename, 
			     int first, int last, 
			     SpaceConfigEnum config ){
  if ( last == -1 ) last = this->size();

  ElectrodeConfigVector profil;
  profil = createPolPolSection( first, last );
  
  return saveSelectedData( filename, profil, 1, config);
}

int DataMap::saveDipolDipolData( const string & filename, 
				 int step, int first, int last, 
				 SpaceConfigEnum config ){

  if ( last == -1 || circle_ ) last = this->size()-1;
  if ( circle_ ) first = 0;

  ElectrodeConfigVector profil;
  profil = createDipolDipolSection( first, last, step, circle_ );
  
  return saveSelectedData( filename, profil, 1, config);
}

ElectrodeConfigVector createPolPolSounding( int a, int first, int last ) {
  ElectrodeConfigVector profil;
  for ( int i = first; i <= last; i ++ ){
    if ( i != a ){
      profil.push_back( ElectrodeConfig( a, -1, i, -1 ) );
    }
  }
  return profil;
}

ElectrodeConfigVector createDipolPolSounding( int a, int b, int first, int last ) {
  ElectrodeConfigVector profil;
  for ( int i = first; i <= last; i ++ ){
    if ( i != a && i != b ){

      profil.push_back( ElectrodeConfig( a, b, i, -1 ) );
    }
  }
  return profil;
}

ElectrodeConfigVector createDipolDipolSounding( int a, int b, int first, int last ) {
  ElectrodeConfigVector profil;
  int m = 0, n = 0;
  for ( int i = first; i <= last-1; i ++ ){
    m = i; 
    n = i+1;
    if ( m != a && m != b && n !=a && n != b ){
      profil.push_back( ElectrodeConfig( a, b, m, n) );
    }
  }
  return profil;
}

ElectrodeConfigVector createPolPolSection( int first, int last ){
  ElectrodeConfigVector profil;
  
  int a = 0, b = -1, m = 0, n = -1;
  for ( int i = first; i < last; i ++ ){
    a = i;
    for ( int j = a + 1; j < last; j ++ ){
      m = j;
      profil.push_back( ElectrodeConfig( a, b, m, n ) );
    }
  }

  return profil;
}

ElectrodeConfigVector createDipolDipolSection( int first, int last, int step, bool circle ){
  // A a B a M a N ; a space
  ElectrodeConfigVector profil;
  //  cout << first << " " << last << " " << step << " " << circle << endl;

  int a = 0, b = 0, m = 0, n = 0, nElecs = last + 1;
  if ( circle ){
    for ( int i = 0; i < nElecs; i ++){
      for ( int j = i; j < nElecs; j ++){
	a = i;
	if ( i == nElecs - 1 ) b = 0;
	else b = i + 1;
	m = j;
	if ( m > nElecs -1 ) m -= nElecs;
	n = m + 1;
	if ( n > nElecs -1 ) n -= nElecs;
	if ( n != a && n != b && m != a && m != b ){
	  profil.push_back( ElectrodeConfig( a, b, m, n ) );
	}
      }
    }
  } else {
    for ( int i = first; i < last - 2 * step; i ++ ){
      a = i;
      b = a + step;
      for ( int j = b + step; j < last; j += step ){
	m = j;
	n = m + step;
	profil.push_back( ElectrodeConfig( a, b, m, n ) );
      }
    }
  }
  return profil;
}

// ElectrodeConfigVector createPolDipolSounding( int first, int last, int step, bool circle ){
// }

ElectrodeConfigVector createWennerSection( int electrodeCount ){
  // A a M a N a B ; a space
  ElectrodeConfigVector data;
  int a = 0, b = 0, m = 0, n = 0, i = 0;
  int level = 0;

  while ( level < floor( electrodeCount / 3.0) ){
    level ++ ;
    i = 1; b = 0;
    while ( b < electrodeCount ){
      a = i; 
      m = i + level; 
      n = i + 2 * level; 
      b = i + 3 * level; 
      data.push_back( ElectrodeConfig(a, b, m, n) );
      i ++;
    }
  }

  //** (N-1)*(N-2)/6
  size_t expectDataSize = (size_t)floor(( electrodeCount - 1 ) * ( electrodeCount - 2 ) / 6.0);
  if ( expectDataSize != data.size() ){
    cerr << expectDataSize << " Datasetlets expected, found = " << data.size() << endl;
  }
  return data;
}

int DataMap::saveDipolDipol( const string & filename, int step, int first, int last, SpaceConfigEnum config ){
  FUTILE														 

  fstream outfile; openOutFile( filename, & outfile );
  int prePrecision = outfile.precision();

  cout << WHERE_AM_I  << "\aWarnung, Checke umbedingt vollraum oder halbraum\a " << endl;
 
  if  ( last == -1 ) last = size() - 1;
  
  int numberoflevels = ( last - first ) - 2;
  double dV = 0.0, dVm = 0.0, dVn = 0.0, rhoa = 0.0;
  double dVRek = 0.0, dVmRek = 0.0, dVnRek = 0.0, rhoaRek = 0.0, Faktor = 0.0, current = 1.0;
  double am, bm, an, bn;
  double amm, amn, bmm, bmn, mma, mmb, nma, nmb;
  double geoAB, geoMN;
  
  int a = 0, b = 0, m = 0, n = 0;
  RealPos mirrorSourceA, mirrorSourceB;
  RealPos mirrorSourceM, mirrorSourceN;
  
  bool fullspace = false;
  if (config == FULLSPACE ) fullspace = true; else fullspace = false;
  if ( fullspace ){
    cout << "using Fullspace 4 * pi." << endl;
  } else {
    cout << "using Halfspace 2 * pi wihout mirror." << endl;
  }
  for ( int spacing = step; spacing <= numberoflevels; spacing += step ){
    for ( int i = first, imax = last - spacing - 2*step ; i <= imax; i += step ){
      a = i;
      b = i + step;
      m = i + step + spacing;
      n = i + step + spacing + step;
      
      //      dV0 = analytischvect[i][m] - analytischvect[i][n];
      if ( dipole_ ){
	dVm = (*this)[a][m];
	dVn = (*this)[a][n];
	
	dVmRek = (*this)[m][a];
	dVnRek = (*this)[m][b];
      } else {
	dVm = (*this)[ a ][ m ] - (*this)[ b ][ m ];
	dVn = (*this)[ a ][ n ] - (*this)[ b ][ n ];
	
	dVmRek = (*this)[ m ][ a ] - (*this)[ m ][ b ];
	dVnRek = (*this)[ n ][ a ] - (*this)[ n ][ b ];
      }
      dV = dVm - dVn;
      dVRek = dVmRek - dVnRek;
	
	//** spiegelung an z = 0;
	mirrorSourceA = sourceNodes_[ a ].pos(); mirrorSourceA.setZ( -1 * mirrorSourceA.z() );
	mirrorSourceB = sourceNodes_[ b ].pos(); mirrorSourceB.setZ( -1 * mirrorSourceB.z() );
	mirrorSourceM = sourceNodes_[ m ].pos(); mirrorSourceM.setZ( -1 * mirrorSourceM.z() );
	mirrorSourceN = sourceNodes_[ n ].pos(); mirrorSourceN.setZ( -1 * mirrorSourceN.z() );

	am = sourceNodes_[ a ].pos().distance( sourceNodes_[ m ].pos() );
	an = sourceNodes_[ a ].pos().distance( sourceNodes_[ n ].pos() );
	bm = sourceNodes_[ b ].pos().distance( sourceNodes_[ m ].pos() );
	bn = sourceNodes_[ b ].pos().distance( sourceNodes_[ n ].pos() );
	amm = mirrorSourceA.distance( sourceNodes_[ m ].pos() );
	amn = mirrorSourceA.distance( sourceNodes_[ n ].pos() );
	bmm = mirrorSourceB.distance( sourceNodes_[ m ].pos() );
	bmn = mirrorSourceB.distance( sourceNodes_[ n ].pos() );
	mma = mirrorSourceM.distance( sourceNodes_[ a ].pos() );
	mmb = mirrorSourceM.distance( sourceNodes_[ b ].pos() );
	nma = mirrorSourceN.distance( sourceNodes_[ a ].pos() );
	nmb = mirrorSourceN.distance( sourceNodes_[ b ].pos() );
      
	if ( fullspace ){
	  geoAB = ( ( 1 / am + 1 / amm ) - ( 1 / bm + 1 / bmm ) ) 
	    - ( ( 1 / an + 1 / amn ) - ( 1 / bn + 1 / bmn ) );
	  rhoa = (dV * 4 * PI_) / ( current * geoAB );
	  geoMN = ( ( 1 / am + 1 / mma ) - ( 1 / bm + 1 / mmb ) ) 
	    - ( ( 1 / an + 1 / nma ) - ( 1 / bn + 1 / nmb ) );
	  rhoaRek = (dVRek * 4 * PI_) / ( current * geoMN );
	} else {
	  geoAB = ( ( 1 / am ) - ( 1 / bm ) ) - ( ( 1 / an ) - ( 1 / bn ) );
	  rhoa = (dV * 2 * PI_) / ( current * geoAB );
	  geoMN = ( ( 1 / am ) - ( 1 / bm ) ) - ( ( 1 / an ) - ( 1 / bn ) );
	  rhoaRek = (dVRek * 2 * PI_) / ( current * geoMN );
	}
	
	//       cout << i << "\t" << i + 1 << "\t" << i + 1 + spacing << "\t" << i + 1 + spacing + 1 << "\t" 
	// 	   << spacing << "\t"
	// 	   << dV << "\t" << dVRek << "\t" 
	// 	   << rhoa << "\t" << rhoaRek << endl;      

	outfile.setf( ios::scientific, ios::floatfield );
	outfile.precision( 14 );
	if ( ! filled( a ) || ! filled( b ) ){ 
	  dV = 0.0; dVRek = 0.0; rhoa = 0.0; rhoaRek = 0.0; 
	}  
	outfile << (int)(i-first)/step << "\t" 
		<< (int)(i-first)/step + 1 << "\t" 
		<< (int)(i-first)/step + 1 + (int)spacing/step << "\t" 
		<< (int)(i-first)/step + 1 + (int)spacing/step + 1 << "\t" 
		<< (int)spacing/step << "\t"
		<< dV << "\t" << dVRek << "\t" 
		<< rhoa << "\t" << rhoaRek << endl;      
      }
    }

  outfile.close();
  return 1;
}

int DataMap::saveDipolDipolKoord( const string & filename, int step, int first, int last ){
 FUTILE 
  fstream outfile; openOutFile( filename, & outfile );
  int prePrecision = outfile.precision();

  if ( last == -1 ) last = size() - 1;
  int numberoflevels = ( last - first ) - 2;
  double dV = 0.0, dVm = 0.0, dVn = 0.0;
  int a = 0, b = 0, m = 0, n = 0;

  for ( int spacing = step; spacing <= numberoflevels; spacing += step ){
    for ( int i = first, imax = last - spacing - 2*step ; i <= imax; i += step ){
      a = i;
      b = i + step;
      m = i + step + spacing;
      n = i + step + spacing + step;

      dVm = (*this)[ a ][ m ] - (*this)[ b ][ m ];
      dVn = (*this)[ a ][ n ] - (*this)[ b ][ n ];

      dV = dVm - dVn;


      if ( ! filled( a ) || ! filled( b ) ){ dV = 0.0; }  
      outfile.setf( ios::fixed, ios::floatfield );
      outfile.precision( prePrecision );
      outfile << sourceNodes_[ a ].x() << "\t" << sourceNodes_[ a ].y() << "\t" 
	      << sourceNodes_[ a ].z() << "\t"
	      << sourceNodes_[ b ].x() << "\t" << sourceNodes_[ b ].y() << "\t"
	      << sourceNodes_[ b ].z() << "\t"
	      << sourceNodes_[ m ].x() << "\t" << sourceNodes_[ m ].y() << "\t"
	      << sourceNodes_[ m ].z() << "\t"
	      << sourceNodes_[ n ].x() << "\t" << sourceNodes_[ n ].y() << "\t"
	      << sourceNodes_[ n ].z() << "\t";
      outfile.setf( ios::scientific, ios::floatfield );
      outfile.precision( 14 );
      outfile << dV << endl;      
    }
  }
  outfile.close();
  return 1;
}

int DataMap::savePolPolKoord( const string & filename, int step, int first, int last ){
											  
FUTILE

  fstream outfile; openOutFile( filename, & outfile );
  int prePrecision = outfile.precision();
  
  if  ( last == -1 ) last = size() -1;
  int numberoflevels = ( last - first );
  double dV = 0.0;
  int a = 0, m = 0;

  for ( int spacing = step; spacing <= numberoflevels; spacing += step ){
    for ( int i = first, imax = last - spacing; i <= imax; i += step ){
      a = i;
      m = i + spacing;
      dV = (*this)[ a ][ m ];

      if ( ! filled( a ) ){ dV = 0.0; }  

      outfile.setf( ios::fixed, ios::floatfield );
      outfile.precision( 2 );
      outfile << sourceNodes_[ a ].x() << "\t" << sourceNodes_[ a ].y() << "\t"
	      << sourceNodes_[ a ].z() << "\t"
	      << sourceNodes_[ m ].x() << "\t" << sourceNodes_[ m ].y() << "\t"
	      << sourceNodes_[ m ].z() << "\t";
	
      outfile.setf( ios::scientific, ios::floatfield );
      outfile.precision( 14 );
      outfile << dV << endl;      
    }
  }
  outfile.close();
  return 1;
}

int DataMap::saveDipolDipolSounding( const string & filename, int nodeA, int nodeB, SpaceConfigEnum config ){;
FUTILE
  cout << WHERE_AM_I << " check K-faktor" << endl;
  fstream outfile; openOutFile( filename, & outfile );
  int prePrecision = outfile.precision();

  RVector rA = vectorOfDistancesFrom( nodeA );
  RVector rB = vectorOfDistancesFrom( nodeB );
  RVector kFaktors( rA );
  RVector rhoA( rA );
  RVector uA = potential( nodeA );
  RVector uB = potential( nodeB );
  RVector uAB = uA - uB;
  RVector uABMN ( uA.size() - 1 ) ;

  double faktor = 0.0;

  bool fullspace = false;
  if (config == FULLSPACE ) fullspace = true; else fullspace = false;
  if ( fullspace ){
    cout << "using Fullspace 4 * pi." << endl;
  } else {
    cout << "using Halfspace 2 * pi wihout mirror." << endl;
  }
  for ( int i = 0, imax = uAB.size()-1; i < imax; i ++ ){
    uABMN[ i ] = uAB[ i ] - uAB[ i + 1 ];
    faktor = 1/rA[ i ] - 1/rB[ i ] - 1/rA[ i + 1 ] + 1/rB[ i + 1 ];
    if( fullspace ){
      rhoA[ i ] = uABMN[ i ] / faktor * 4.0 * PI_;
    } else {
      rhoA[ i ] = uABMN[ i ] / faktor * 2.0 * PI_;
    }
  }

  int nodeM = 0;
  for ( int i = 0, imax = uABMN.size(); i < imax; i ++ ){
    nodeM = i;
    
    if ( nodeM != nodeA ){
      outfile.setf( ios::fixed, ios::floatfield );
      outfile.precision( 2 );
      outfile << sourceNodes_[ nodeA ].pos().x() << "\t"
	      << sourceNodes_[ nodeA ].pos().y() << "\t"
	      << sourceNodes_[ nodeA ].pos().z() << "\t"
	      << sourceNodes_[ nodeB ].pos().x() << "\t"
	      << sourceNodes_[ nodeB ].pos().y() << "\t"
	      << sourceNodes_[ nodeB ].pos().z() << "\t"
	      << sourceNodes_[ nodeM ].pos().x() << "\t"
	      << sourceNodes_[ nodeM ].pos().y() << "\t"
	      << sourceNodes_[ nodeM ].pos().z() << "\t"
	      << sourceNodes_[ nodeM +1 ].pos().x() << "\t"
	      << sourceNodes_[ nodeM +1 ].pos().y() << "\t"
	      << sourceNodes_[ nodeM +1 ].pos().z() << "\t";
      outfile.setf( ios::scientific, ios::floatfield );
      outfile.precision( 14 );
      outfile << uABMN[ i ] << "\t" << rhoA[ i ] << endl;      
    }
  }
  outfile.close();
  return 1;
}

int DataMap::savePolPolSounding( const string & filename, int nodeA, int step, int first, int last, 
				 SpaceConfigEnum config ){;
FUTILE
  fstream outfile; openOutFile( filename, & outfile );
  int prePrecision = outfile.precision();
  
  if  ( last == -1 ) last = size() -1;
  
  int nodeM = 0;
  RVector uA = potential( nodeA );
  RVector rhoA( uA / standardisedPotential( sourceNodes_[ nodeA ].pos(), sourceNodes_, config) );
  //  rhoA.show();
  //standardisedPotential( sourceNodes_[ nodeA ].pos(), sourceNodes_, config).show();
  //uA.show();
  
  double uARec = 0.0, rhoARec = 0.0;
  for ( int i = first, imax = last + 1; i < imax; i += step ){
    nodeM = i;
    
//     uARec = (*this)[ i ][ nodeA ];
//     rhoARec = ( uARec / kFaktors[ i ] ) * 2.0 * PI_;
    if ( nodeM != nodeA ){
      outfile.setf( ios::fixed, ios::floatfield );
      outfile.precision( 2 );
      outfile << sourceNodes_[ nodeA ].pos().x() << "\t"
	      << sourceNodes_[ nodeA ].pos().y() << "\t"
	      << sourceNodes_[ nodeA ].pos().z() << "\t"
	      << sourceNodes_[ nodeM ].pos().x() << "\t"
	      << sourceNodes_[ nodeM ].pos().y() << "\t"
	      << sourceNodes_[ nodeM ].pos().z() << "\t";
      outfile.setf( ios::scientific, ios::floatfield );
      outfile.precision( 14 );
      outfile << uA[ i ] << "\t" << uARec << "\t"
	      << rhoA[ i ] << "\t" << rhoARec << endl;      
    }
  }

  outfile.setf( ios::fixed, ios::floatfield );
  outfile.precision( prePrecision );
  outfile.close();
  return 1;
}

// void savePolPolMap( RVector & pot, Mesh2D & mesh, const string & filename, 
// 		    map<double, int> & nodeMap, double current, int s1){

//   fstream of; openOutFile(filename, &of );
//   double rho = 0, r = 0;
  
//   of.setf(ios::scientific);
//   for ( map<double, int>::iterator it = nodeMap.begin(); it != nodeMap.end(); it ++ ){
//     r = mesh.node( (*it).second ).distance( mesh.node( s1 ) );
//     //cout << mesh.node( (*it).second ).x() << "\t " <<mesh.node( (*it).second ).z() << "\t" << r << endl;
//     rho = pot[ (*it).second ] * 2 * PI_ * r / current ;
//     cout << rho << endl;
//     of << (*it).first << "\t" ;
//     of << rho << endl;
//   }
//   of.unsetf(ios::scientific);
// }

// void savePolPolVector( RVector & pot, Mesh2D & mesh, const string & filename, 
// 		       vector<int> & vec, double current, int s1){

//   fstream of; openOutFile(filename, &of );
//   double rho = 0, r = 0;
  
//   of.setf(ios::scientific);
//   for ( vector<int>::iterator it = vec.begin(); it != vec.end(); it ++ ){
//     r = mesh.node( (*it) ).distance( mesh.node( s1 ) );
//     cout << mesh.node( (*it) ).x() << "\t " <<mesh.node( (*it) ).z() << "\t" << r << endl;
//     rho = pot[ (*it) ] * 2 * PI_ / ( current * (1. / r) );
//     of << (*it)-vec[0] << "\t" << rho << endl;
//   }
//   of.unsetf(ios::scientific);
// }


// void savePotentialVector( RVector & x, const string & filename, vector<int> & vec){
//   fstream of; openOutFile(filename, &of );
  
//   of.setf(ios::scientific);
//   for (vector<int>::iterator it = vec.begin(); it != vec.end(); it ++){
//     of << (*it)-vec[0] << "\t" << x[ (*it) ] << endl;
//   }
//   of.unsetf(ios::scientific);
//   of.close();
// }

// void savePotentialMap(RVector & x, Mesh2D & mesh, const string & filename, 
// 		   map<double, int> & nodeMap, char koord){
//   fstream of; openOutFile(filename, &of );

//   of.setf(ios::scientific);
//   for (map<double, int>::iterator it = nodeMap.begin(); it != nodeMap.end(); it ++){
//     if (koord == 'x')
//     of << mesh.node((*it).second).x() << "\t";
//     else if (koord == 'z') of << mesh.node((*it).second).z() << "\t";

//     //    of << mesh.node((*it).second).x() << "\t" << mesh.node((*it).second).z() << "\t";

//     of << x[(*it).second] << endl;

//   }
//   of.unsetf(ios::scientific);
//   of.close();
// }

// void saveDipolPol(vector<RVector> & xlist, Mesh2D & mesh, 
// 		const string & filename, map<double, int> & nodeMap, double current, int s1, int s2){
//   fstream of; openOutFile(filename, &of );

//   double r1, r2, konst;

//   of.setf(ios::scientific);
//   for (map<double, int>::iterator it = nodeMap.begin(); it != nodeMap.end(); it ++){
//     if ((*it).second != s1 && (*it).second != s2){
//       r1 = mesh.node( (*it).second ).distance( mesh.node(s1) );
//       r2 = mesh.node( (*it).second ).distance( mesh.node(s2) );
//       konst = 1/(2 *PI_ ) * ( (1 / r1) - (1 / r2));
      
//       if (r1 != r2){
// 	of << mesh.node((*it).second).x() << "\t"
// 	   << (xlist[0][(*it).second] - xlist[1][(*it).second])/(current * konst) << endl;
//       }
//     }
//   }
  
//   of.unsetf(ios::scientific);
//   of.close();
// }

/*
$Log: datamap.cpp,v $
Revision 1.49  2008/12/11 15:14:13  carsten
*** empty log message ***

Revision 1.48  2007/12/13 18:42:16  carsten
*** empty log message ***

Revision 1.47  2007/09/17 19:04:26  carsten
*** empty log message ***

Revision 1.46  2007/06/12 12:51:35  carsten
*** empty log message ***

Revision 1.45  2007/05/22 18:50:04  carsten
*** empty log message ***

Revision 1.44  2007/03/09 13:24:23  carsten
*** empty log message ***

Revision 1.43  2007/03/09 13:22:33  carsten
*** empty log message ***

Revision 1.42  2007/03/07 16:38:14  carsten
*** empty log message ***

Revision 1.41  2007/01/25 17:38:13  carsten
*** empty log message ***

Revision 1.40  2006/11/08 19:53:24  carsten
*** empty log message ***

Revision 1.38  2006/10/10 09:46:01  carsten
*** empty log message ***

Revision 1.37  2006/09/11 13:21:01  carsten
*** empty log message ***

Revision 1.36  2006/08/21 16:15:03  carsten
*** empty log message ***

Revision 1.33  2006/08/20 20:56:31  carsten
*** empty log message ***

Revision 1.32  2006/08/02 18:24:19  carsten
*** empty log message ***

Revision 1.31  2006/07/20 16:05:47  carsten
*** empty log message ***

Revision 1.30  2006/07/18 14:08:25  carsten
*** empty log message ***

Revision 1.29  2006/06/29 12:47:27  carsten
*** empty log message ***

Revision 1.28  2005/11/23 15:46:51  carsten
*** empty log message ***

Revision 1.27  2005/11/04 20:21:36  carsten
*** empty log message ***

Revision 1.26  2005/10/24 09:52:08  carsten
*** empty log message ***

Revision 1.25  2005/10/12 14:41:56  carsten
*** empty log message ***

Revision 1.24  2005/09/12 10:38:36  carsten
*** empty log message ***

Revision 1.23  2005/08/24 20:22:11  carsten
*** empty log message ***

Revision 1.22  2005/07/13 14:02:35  carsten
*** empty log message ***

Revision 1.20  2005/07/13 12:27:35  carsten
*** empty log message ***

Revision 1.19  2005/07/12 13:32:12  carsten
*** empty log message ***

Revision 1.18  2005/07/06 13:15:20  carsten
*** empty log message ***

Revision 1.17  2005/07/04 15:11:22  carsten
*** empty log message ***

Revision 1.16  2005/06/30 17:32:16  carsten
*** empty log message ***

Revision 1.15  2005/06/23 20:15:32  carsten
*** empty log message ***

Revision 1.14  2005/06/22 20:39:02  carsten
*** empty log message ***

Revision 1.13  2005/04/26 10:40:23  carsten
*** empty log message ***

Revision 1.12  2005/04/12 11:41:09  carsten
*** empty log message ***

Revision 1.11  2005/04/08 14:09:26  carsten
*** empty log message ***

Revision 1.10  2005/03/29 16:57:46  carsten
add inversion tools

Revision 1.9  2005/03/24 14:39:03  carsten
*** empty log message ***

Revision 1.8  2005/03/18 19:47:17  carsten
*** empty log message ***

Revision 1.7  2005/03/08 18:20:35  carsten
*** empty log message ***

Revision 1.6  2005/03/08 17:35:18  carsten
*** empty log message ***

Revision 1.4  2005/02/18 14:06:19  carsten
*** empty log message ***

Revision 1.3  2005/02/07 13:33:13  carsten
*** empty log message ***

Revision 1.2  2005/01/13 20:10:42  carsten
*** empty log message ***

Revision 1.1  2005/01/04 19:22:30  carsten
add in cvs

*/
