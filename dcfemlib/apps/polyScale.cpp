// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include <dcfemlib.h>
#include <longoptions.h>
#include <iostream>

#include <domain2d.h>
#include <domain3d.h>

using namespace std;
using namespace DCFEMLib;

int main( int argc, char * argv[] ){

  LongOptionsList lOpt;
  lOpt.setLastArg("[-Lx:y:z:] polygon-filename");
  lOpt.setDescription( (string) "Scale a polygonal domain: " );
  lOpt.insert( "help", no_argument, 'h', "This help" );
  lOpt.insert( "verbose", no_argument, 'v', "Verbose mode" );
  lOpt.insert( "local", no_argument, 'L', "Transform in local space" );
  lOpt.insert( "x", required_argument, 'x', "double: scale x [1.0]" );
  lOpt.insert( "y", required_argument, 'y', "double: scale y [1.0]" );
  lOpt.insert( "z", required_argument, 'z', "double: scale z [1.0]" );

  bool help = false, verbose = false, local = false;
  string domainName( NOT_DEFINED );

  double x = 1.0, y = 1.0, z = 1.0;
  int option_char = 0, option_index = 0, tracedepth = 0;
  while ( ( option_char = getopt_long( argc, argv, "?hvL"
				       "x:y:z:",
				       lOpt.long_options(), & option_index ) ) != EOF){
    switch ( option_char ) {
    case '?': lOpt.printHelp( argv[0] ); return 1; break;
    case 'h': lOpt.printHelp( argv[0] ); return 1; break;
    case 'v': verbose = true; break;
    case 'L': local = true; break;
    case 'x': x = atof( optarg ); break;
    case 'y': y = atof( optarg ); break;
    case 'z': z = atof( optarg ); break;
    default : cerr << "default value not defined" << endl;
    }
  }
  if ( argc < 2 ) { lOpt.printHelp( argv[0] ); return 1; }

  domainName = argv[argc-1];
  string domainFileName( domainName.substr( 0, domainName.rfind( ".poly" ) ) + ".poly" );

  int dimension = findDomainDimension( domainFileName );

  BaseMesh *domain;
  switch ( dimension ){
  case 2: domain = new Domain2D();
    break;
  case 3: domain = new Domain3D();
    break;
  default: return -1;
  }

  domain->load( domainFileName );

  RealPos avPos( averagePosition( domain->nodePositions() ) );
  if ( local )  domain->translate( avPos * -1.0 );
  domain->scale( x, y, z );
  if ( local )  domain->translate( avPos );

  domain->save( domainFileName );

  return 0;

}
